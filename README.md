# PBD2019-63160137

###Email: 
* ji4607@student.uni-lj.si

###Project by: 
* Jan Ivanović

###Project Description:
Creating a complex Sport android application. The user will be able to get feedback and details of his workout.

####Sprint #4 - Compete Mode

Us humans always want to get better and advance in life. After a period of time we will want to achive more than we did and won't be satisfied with the results that we got in the past. But a large amout of people need help getting motivated to do better. That is why, I implemented a new update to the app RunsUp! which is the "Compete Mode". With it, you will be able to select one of your previous workouts and compete with it. You will also be notified of your competition progress while running. The fact that you are now able to compete with yourself, brings alot more meaning to your workouts. When you win and beat your previous result you get a sense of self-growth, which is one of the most psychologicly valued attributes for us as human. It gets you motivated and craving for more succsess. Not just the success of beating your next best run but the success outside the app. It works as a domino effect of motivation, because normaly when you acomplish something, you get motivated and want to acomplish something else. 
I was aiming for a simple but helpful way to add a great motivatior which has alot more value and influence on the user, even outside the app and the "Compete mode" seemed a perfect way to achive this.

The update tries to create a minimal user effort when using "Compete mode" so you can acctually concentrate on running and winning the competition. Therefore you don't need to check your phone while running to see if you are winning or loosing, but the app will automaticly notify you. When your pace will drop lower than the pace of your competing run, the device will vibrate so you know you have to step it up. The same goes if you start winning. With this mechanic you get a feeling of what your tempo should be if you want to win. This will motivate the user without disturbing him and taking him any unnecessary time while running. The competition will be over when you reach the same distance or duration as you did in the competing workout. You will be notified with a different kind of vibration so you know when to stop. After you get the result of the competition (Won/Lost) you will be able to continue the workout if you want or end it. The "Compete mode" is most efficient to use when you choose the same route that you took as in your competing workout so that you get the most detailed and real results.

Furthermore, while in "Compete mode", the update contains the option to use "Voice support". This option was made for two reasons. The first one is revolving around the postion of your device while using the app. Maybe the user will have its device somewhere, where he cannot feel the vibration of the phone. Thus with the use of "Voice support" this problem will be solved. It will notify the user of his progres while running just like the vibrations, so he doesn't have worry about looking at the device. The second reason is motivational. If you use "Voice support" you will also get to hear motivational sences while runnig. There are more then 10+ unique sentences included in the update. Their appearence is not randomized but it is carefully picked. For example if you are slowing down and your pace is dropping, you will recive a different motivational sentence than if you were to speed up. It is made to motivate the user as much as possible and give him that extra push that he needs when he is starting to feel tired or to provide it as sort of a reward for doing so well. 

-----------------
An example of the updated app in use:

**STEP 1:** We enable Compete mode and select one of our previous workouts, that we want to compete againts (Voice support is optional).

![Step 1](img/step_1.jpg)

**STEP 2:** After selecting it, we get a better representation of the workout.

![Step 2](img/step_2.jpg)

**STEP 3:** You proceed to use the app normlly (Start workout) and get notified of the results mid-workout.

![Step 3](img/step_3_behind.jpg) ![Step 3](img/step_3_ahead.jpg)

**STEP 4:** Reaching the end and recieving the results about your competition. 

![Step 4](img/step_4.jpg)

###Libraries:
