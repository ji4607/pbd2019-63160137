package si.uni_lj.fri.pbd2019.runsup;


import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;

import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;


public class CompeteActivity extends AppCompatActivity implements DialogCompete.DialogListener{

    private static final String TAG = "CompeteFragment";
    private  Long workoutId;
    private Button compete_btn;
    private SharedPreferences mSharedPref;
    private SharedPreferences.Editor mEditor;
    private TextView competitor_text;
    MainHelper mainHelper = new MainHelper();
    private Switch voice_toggle;
    private Switch compete_toggle;

    //Dao objekti za bazo
    Dao<Workout, Long> workoutDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compete);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //INICIALIZIRAJ DAO
        workoutDao = null;
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        try {
            workoutDao = databaseHelper.workoutDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        mSharedPref = getSharedPreferences("SharedPref", Context.MODE_PRIVATE);
        mEditor = mSharedPref.edit();

        competitor_text = (TextView) findViewById(R.id.textview_selected_workout);
        voice_toggle = (Switch) findViewById(R.id.voice_support_switch);
        compete_toggle = (Switch) findViewById(R.id.compete_mode_switch);

        compete_btn = findViewById(R.id.select_workout_btn);
        compete_btn.setActivated(false);

        Boolean competeSwitch = mSharedPref.getBoolean("competeSwitch", false);
        Boolean voiceSwitch = mSharedPref.getBoolean("voiceSwitch", false);
        if(competeSwitch){
            compete_toggle.setChecked(true);
            compete_btn.setActivated(true);

        }
        if(voiceSwitch){
            voice_toggle.setActivated(true);
        }
        changeText();








        compete_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!compete_btn.isActivated()) {
                    Toast.makeText(getApplicationContext(), "Enable compete mode first", Toast.LENGTH_SHORT).show();
                    return;
                }else{
                    openDialog();
                }

            }
        });

        voice_toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Boolean competeSwitch = mSharedPref.getBoolean("competeSwitch", false);
                    if(!competeSwitch){
                        Toast.makeText(getApplicationContext(), "Enable compete mode first", Toast.LENGTH_SHORT).show();
                        voice_toggle.setChecked(false);
                        mEditor.putBoolean("voiceSwitch", false);
                        mEditor.commit();
                    }else{
                        mEditor.putBoolean("voiceSwitch", true);
                        mEditor.commit();
                        //OH BOY, YOU ARE IN FOR A TREAT
                        MediaPlayer mp = MediaPlayer.create(CompeteActivity.this, R.raw.voice_toggle);
                        mp.start();
                    }
                } else {
                    mEditor.putBoolean("voiceSwitch", false);
                    mEditor.commit();
                }
            }
        });

        compete_toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mEditor.putBoolean("competeSwitch", true);
                    mEditor.commit();
                    compete_btn.setActivated(true);
                    compete_btn.setVisibility(View.VISIBLE);
                } else {
                    mEditor.putBoolean("competeSwitch", false);
                    mEditor.putBoolean("voiceSwitch", false);
                    mEditor.putLong("workoutId", -1);
                    mEditor.commit();
                    applyText();
                    compete_btn.setActivated(false);
                    voice_toggle.setChecked(false);
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        compete_btn.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void openDialog() {
        DialogCompete mDialog = new DialogCompete();
        mDialog.show(getSupportFragmentManager(), "Dialog");
    }

    @Override
    public void applyText() {
        workoutId = mSharedPref.getLong("workoutId", -1);
        if(workoutId != -1 && workoutId != null){
            try{
                Workout workout = workoutDao.queryForId(workoutId);

                competitor_text.setVisibility(View.VISIBLE);
                competitor_text.setText(setInfo(workout));

            } catch (SQLException e){
                e.printStackTrace();
            }
        }else {
            competitor_text.setText("No workout selected");
        }

    }

    public void changeText(){
        workoutId = mSharedPref.getLong("workoutId", -1);

        if(workoutId != -1 && workoutId != null){
            try{
                Workout workout = workoutDao.queryForId(workoutId);

                competitor_text.setVisibility(View.VISIBLE);
                competitor_text.setText(setInfo(workout));

            } catch (SQLException e){
                e.printStackTrace();
            }
        }else {
            competitor_text.setText("No workout selected");
        }
    }

    public String setInfo(Workout workout){

        String sportActivity = "";
        String labelDistanceKm = getString(R.string.all_labeldistanceunitkilometers);
        String labelMin = getString(R.string.all_min);

        if (workout.getSportActivity() == 0) {
            sportActivity = "run";
        } else if (workout.getSportActivity() == 1) {
            sportActivity = "walk";
        } else if(workout.getSportActivity() == 2){
            sportActivity = "cycle";
        }
        String durationString = mainHelper.timeChange(workout.getDuration() / 1000);
        String distanceString = mainHelper.formatDistance(workout.getDistance());
        String caloriesString = String.valueOf((int) Math.floor(workout.getTotalCalories()));
        String paceString = mainHelper.formatPace(workout.getPaceAvg());

        return ("Your competitor workout is " + workout.getTitle() +".\nTo beat it you will have to " + sportActivity + " " + distanceString + " " + labelDistanceKm + " in " +
                durationString + " with an avrage pace of " + paceString + " " + labelMin + "/" + labelDistanceKm + ".");

    }
}
