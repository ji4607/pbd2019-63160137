package si.uni_lj.fri.pbd2019.runsup;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;

import java.net.URI;

import jp.wasabeef.glide.transformations.RoundedCornersTransformation;
import si.uni_lj.fri.pbd2019.runsup.fragments.AboutFragment;
import si.uni_lj.fri.pbd2019.runsup.fragments.CompeteFragment;
import si.uni_lj.fri.pbd2019.runsup.fragments.HistoryFragment;
import si.uni_lj.fri.pbd2019.runsup.fragments.StopwatchFragment;
import si.uni_lj.fri.pbd2019.runsup.settings.SettingsActivity;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    private static final String TAG = "MainActivity";



    private ImageView mProfilePic;
    private TextView mProfileName;
    private SharedPreferences mSharedPref;
    private SharedPreferences.Editor mEditor;
    private boolean userSignedIn;
    private Uri mImageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mProfilePic = (ImageView) findViewById(R.id.menu_loggedInUserImage);
        mProfileName = (TextView) findViewById(R.id.menu_loggedInUserFullName);

        userSignedIn = false;




        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headView = navigationView.getHeaderView(0);

        mSharedPref = getSharedPreferences("SharedPref", Context.MODE_PRIVATE);


        //DEFAULT FRAGEMENT
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, new StopwatchFragment()).commit();
        navigationView.setCheckedItem(R.id.nav_workout);



        mProfilePic = headView.findViewById(R.id.menu_loggedInUserImage);
        mProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });
        mProfileName = headView.findViewById(R.id.menu_loggedInUserFullName);
        mProfileName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();

        String username = mSharedPref.getString("username", String.valueOf(R.string.all_unknownuser));
        String imageUri = mSharedPref.getString("url", "noImage");
        try {
            //mImageUri  = Uri.create(mSharedPref.getString("url", "defaultString"));
        } catch (IllegalArgumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if(mSharedPref.getBoolean("userSignedIn",userSignedIn)){
            mProfileName.setText(username);

            Glide.with(MainActivity.this)
                    .load(imageUri)
                    .override(216, 216)
                    .into(mProfilePic);
            Log.d(TAG, "SEM V IFUUUU: " + mSharedPref.getString("username", String.valueOf(R.string.all_unknownuser)));

        }else{
            mProfileName.setText(getString(R.string.all_unknownuser));
            Log.d(TAG, "NISEM V IFU: " + String.valueOf(R.string.all_unknownuser));
        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(intent);

        }else if(id == R.id.action_delete){

        }

        return super.onOptionsItemSelected(item);
    }*/




    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_workout) {
            getSupportActionBar().setTitle("RunsUp!");
            //LOAD STOPWATCH FRAGMENT
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, new StopwatchFragment()).commit();

        } else if (id == R.id.nav_history) {
            getSupportActionBar().setTitle("History");
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, new HistoryFragment()).commit();
            //LOAD HISTORY FRAGMENT

        } else if (id == R.id.nav_settings) {
            //OPENS SETTINGS ACTIVITY
            Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_about) {
            getSupportActionBar().setTitle("About");
            //LOAD ABOUT FRAGMENT
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, new AboutFragment()).commit();

        } else if (id == R.id.nav_compete) {
            //OPENS COMPETE ACTIVITY
            Intent intent = new Intent(MainActivity.this, CompeteActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

}
