package si.uni_lj.fri.pbd2019.runsup.helpers;

import android.util.Log;

import java.util.List;

import static java.lang.Float.NaN;

public final class SportActivities {

    private static final String TAG = "Sport";
    public static final int RUNNING = 0;
    public static final int WALKING = 1;
    public static final int CYCLING = 2;

    //RUNNING
    static double[][] Rtable = {{6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0},
            {9.8, 11.0, 11.8, 12.8, 14.5, 16.0, 19.0, 19.8, 23.0}};
    static double Rmet = 1.535353535;

    //WALKING
    static double[][] Wtable = {{1.0, 2.0, 3.0, 4.0},
            {2.0, 2.8, 3.1, 3.5}};
    static double Wmet = 1.14;

    //CYCLING
    static double[][] Ctable = {{10.0, 12.0, 14.0, 16.0, 18.0, 20.0},
            {6.8, 8.0, 10.0, 12.8, 13.6, 15.8}};
    static double Cmet = 0.744444444;

    //kilos
    static float defaultWeight = 60f;

    public static void main (String [] args){
        System.out.println(getMET(0, 6.0f));
    }

    //Returns MET value for activity
    //@param acrivityType - sport activity type(0-run, 1-walk, 2-cycle)
    //@param speed - speed in m/s
    public static double getMET(int activityType, Float speed){

        double MET = -1.0;

        //spremeni v pravo enoto
        MainHelper mainHelper = new MainHelper();
        double speedToMPH = mainHelper.mpsToMiph((double)speed);

        //zaokrozi navzdol
        double speedCeil = Math.ceil(speedToMPH);

        switch (activityType) {
            case 0:
                for(int i = 0; i < Rtable[0].length; i++){
                    if(Rtable[0][i] == speedCeil){
                        MET = Rtable[1][i];
                        break;
                    }
                }
                break;
            case 1:
                for(int i = 0; i < Wtable[0].length; i++){
                    if(Wtable[0][i] == speedCeil){
                        MET = Wtable[1][i];
                        break;
                    }
                }
                break;
            case 2:
                for(int i = 0; i < Ctable[0].length; i++){
                    if(Ctable[0][i] == speedCeil){
                        MET = Ctable[1][i];
                        break;
                    }
                }
                break;
        }

        if(MET == -1.0){
            MET = calcMET(activityType, speedCeil);
        }
        return MET;
    }

    //vrni MET, ce ne ustreza nobeni
    public static double calcMET(int activityType, double speed){

        double MET = 0.0;
        switch (activityType) {
            case 0:
                MET = (speed * Rmet);
                break;
            case 1:
                MET = (speed * Wmet);
                break;
            case 2:
                MET = (speed * Cmet);
                break;
        }

        return MET;
    }

    //returns final calories computed from data provided (returns value in kcal)
    //@param acrivityType - sport activity type(0-run, 1-walk, 2-cycle)
    //@param weight - weight in kg
    //@param speedList - list of all speed values recorded (unit = m/s)
    //@param timeFillingSpeedListInHours - time of collecting speed list (duration of sport activity from first to last speedPoint in speedList)
    public static double countCalories(int sportActivity, float weight, List<Float> speedList, double timeFillingSpeedListInHours){

        //AVG HITROST
        float avgSpeed = 0f;
        for(int i = 0; i < speedList.size(); i++){
            avgSpeed += speedList.get(i);
        }
        avgSpeed = avgSpeed/speedList.size();
        //MET
        double MET = getMET(sportActivity, avgSpeed);
        //TEŽA
        if((weight == 0.0) || (weight == NaN)){
            weight = defaultWeight;
        }
        //ČAS TRAJANJA JE PODAN V ARGUMENTIH
        Log.d(TAG, "MET: " + MET + " weight: " + weight + " timeFillingSpeed: " + timeFillingSpeedListInHours );
        double kcal = MET * weight * timeFillingSpeedListInHours;

        return kcal;

    }
}
