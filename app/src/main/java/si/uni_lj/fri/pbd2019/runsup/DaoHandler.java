package si.uni_lj.fri.pbd2019.runsup;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.android.apptools.OrmLiteBaseActivity;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;

import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;

public class DaoHandler extends OrmLiteBaseActivity<DatabaseHelper> {

    private static final String TAG = "DaoHandler";

    public DaoHandler() {
    }

    private void addWorkout(){
        Dao<Workout, Long> workoutDao = null;
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        try {
            workoutDao = databaseHelper.workoutDao();
        } catch (SQLException e){
            e.printStackTrace();
        }

        Workout workout = new Workout("Workout", 0);
        try {
            workoutDao.create(workout);

        } catch (SQLException e){
            e.printStackTrace();
        }
    }
}
