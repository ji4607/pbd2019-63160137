package si.uni_lj.fri.pbd2019.runsup;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.location.Location;
import android.media.MediaPlayer;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.query.In;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.fragments.HistoryFragment;
import si.uni_lj.fri.pbd2019.runsup.fragments.StopwatchFragment;
import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;
import si.uni_lj.fri.pbd2019.runsup.services.TrackerService;


public class ActiveWorkoutMapActivity extends FragmentActivity implements OnMapReadyCallback {

    private static final String TAG = "ActiveMap";
    private GoogleMap mMapActive;
    private IntentFilter mfilter = new IntentFilter("si.uni_lj.fri.pbd2019.runsup.TICK");

    private Long workoutID;
    private static Location curretnLocation;
    private static Location lastLocation;
    private Location intentLocation;
    private Marker markerName;

    //Dao objekti za bazo
    private Dao<Workout, Long> workoutDao;
    private Dao<GpsPoint, Long> gpsPointsDao;
    private DatabaseHelper databaseHelper;

    private Button btn_back;
    private MarkerOptions place1, place2, markerPostion;
    Polyline currentPolyline;
    private long pathTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_active_workout_map);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment_activeworkoutmap_map);
        mapFragment.getMapAsync(this);

        Intent intent = getIntent();
        intentLocation = (Location) intent.getParcelableExtra("location");

        workoutDao = null;
        gpsPointsDao = null;
        databaseHelper = OpenHelperManager.getHelper(ActiveWorkoutMapActivity.this, DatabaseHelper.class);
        try {
            workoutDao = databaseHelper.workoutDao();
            gpsPointsDao = databaseHelper.gpsPointDao();
        }catch (SQLException e) {
            e.printStackTrace();
        }


        place2 = new MarkerOptions().position(new LatLng(27.667491, 85.3208583)).title("Location 2");
        markerPostion = null;
        /*
        String url = getUrl(place1.getPosition(), place2.getPosition(), "walking");
        new FetchURL(ActiveWorkoutMapActivity.this).execute(url, "walking");
        */
        btn_back = (Button) findViewById(R.id.button_activeworkoutmap_back);

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMapActive = googleMap;
        Log.d(TAG, "ON MAP READY");
        if(lastLocation != null){
            LatLng currentLatLng = new LatLng(lastLocation.getLatitude(),
                    lastLocation.getLongitude());
            place1 = new MarkerOptions().position(currentLatLng).title("Current position");
            mMapActive.addMarker(place1).showInfoWindow();
            CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(place1.getPosition(), 18);
            mMapActive.animateCamera(yourLocation);

        } else if(intentLocation != null){
            LatLng currentLatLng = new LatLng(intentLocation.getLatitude(),
                    intentLocation.getLongitude());
            place1 = new MarkerOptions().position(currentLatLng).title("Current position");
            mMapActive.addMarker(place1).showInfoWindow();
            CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(place1.getPosition(), 18);
            mMapActive.animateCamera(yourLocation);
        }
        /*if(pathTime == 15000){
            Log.d(TAG, "SEM V DRAW PATH");
            drawPath();
        }*/

        /*
        Intent intent = getIntent();
        curretnLocation = intent.getParcelableExtra("location");

        if(curretnLocation != null){
            LatLng currentLatLng = new LatLng(curretnLocation.getLatitude(),
                    curretnLocation.getLongitude());
            markerName = mMapActive.addMarker(new MarkerOptions().position(currentLatLng).title("Current Position"));
            CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(markerName.getPosition(), 15);
            mMapActive.animateCamera(yourLocation);
        }
        */
        //drawPath();
        //mMapActive.moveCamera(CameraUpdateFactory.newLatLng(markerName.getPosition()));

    }

    public void drawPath(){
        List<GpsPoint> realList = null;
        try{
            List<GpsPoint> gpsPointList = gpsPointsDao.queryForAll();

            for(GpsPoint x : gpsPointList){
                Log.d(TAG, "DODAM V REAL LIST");
                if(x.getWorkout().getId() == workoutID){
                    realList.add(x);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if(realList != null){
            GpsPoint current = null;
            GpsPoint before = null;
            for(int i = 1; i < realList.size(); i++){
                current = realList.get(i);
                before = realList.get(i-1);
                LatLng currentLatLng = new LatLng(current.getLatitude(),
                        current.getLongitude());
                LatLng beforeLatLng = new LatLng(before.getLatitude(),
                        before.getLongitude());

                mMapActive.addPolyline(new PolylineOptions().add(currentLatLng, beforeLatLng).width(10).color(Color.BLUE));
            }
        }


    }

    private BroadcastReceiver mRecevier = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "BROADCAST RECIVED");
            Bundle extras = intent.getExtras();

            extras.getInt("state");
            workoutID = extras.getLong("workoutID");
            pathTime = extras.getLong("pathTime");

            curretnLocation = intent.getParcelableExtra("location");

            //ZA PRVI LOCATION UPDATE
            if(curretnLocation != null){
                lastLocation = curretnLocation;
                LatLng currentLatLng = new LatLng(curretnLocation.getLatitude(),
                        curretnLocation.getLongitude());
                mMapActive.clear();
                markerPostion = new MarkerOptions().position(currentLatLng).title("Current position");
                mMapActive.addMarker(markerPostion).showInfoWindow();
                CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(markerPostion.getPosition(), 18);
                mMapActive.animateCamera(yourLocation);


            }
            Log.d(TAG, "PATH TIME " + pathTime);




            //UPDEJTAJ  SPROTI DA CE SLUCAJNO CRKNE UMES TA WORKOUT AL CE DA CYCLING ALKEJ


            //DOBIŠ TICK IN UPDEJTAJ
            //updateUI();

        }
    };

    @Override
    public void onPause() {
        unregisterReceiver(mRecevier);

        super.onPause();
    }

    @Override
    public void onResume() {
        registerReceiver(mRecevier, mfilter);
        //to si dodau
        super.onResume();
    }
}
