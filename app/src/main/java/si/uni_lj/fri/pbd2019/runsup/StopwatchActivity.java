package si.uni_lj.fri.pbd2019.runsup;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;
import si.uni_lj.fri.pbd2019.runsup.services.TrackerService;

import static si.uni_lj.fri.pbd2019.runsup.services.TrackerService.REQUEST_ID_LOCATION_PERMISSIONS;

public class StopwatchActivity extends AppCompatActivity {

    private static final String TAG = "StopwatchActivity";
    private static ArrayList<List<Location>> postitionLists;

    MainHelper mainHelper = new MainHelper();

    //UI Komponente
    private Button btn_sport, btn_end, btn_active_map, btn_select_sport;
    private String[] sportSelection;

    //SPREMENLJIVKE
    private int sportActivity, state, sportType;
    private long duration;
    private double distance, pace, calories, avgSpeed;
    private static ArrayList<Location> positionList;

    private IntentFilter mfilter = new IntentFilter("si.uni_lj.fri.pbd2019.runsup.TICK");
    //public TrackerService mService = new TrackerService();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stopwatch);
        //preveri če je premission usposobljen cene uprasi
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED) {
            askPermission();
        }
        state = 0;
        postitionLists = new ArrayList<List<Location>>();
        avgSpeed = 0;
        sportActivity = 0;

        sportSelection = getResources().getStringArray(R.array.select_sport_array);
        btn_end = (Button) findViewById(R.id.button_stopwatch_endworkout);
        btn_sport = (Button) findViewById(R.id.button_stopwatch_start);
        btn_active_map = (Button) findViewById(R.id.button_stopwatch_activeworkout);
        btn_select_sport = (Button) findViewById(R.id.button_stopwatch_selectsport);

        btn_select_sport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //testAlert();
                radioButtonAlert();
            }
        });

        btn_active_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StopwatchActivity.this, ActiveWorkoutMapActivity.class);
                startActivity(intent);
            }
        });

        btn_end.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StopwatchActivity.this, TrackerService.class);
                intent.setAction("si.uni_lj.fri.pbd2019.runsup.COMMAND_PAUSE");
                startService(intent);
                alertCreate();
            }
        });

        btn_sport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String msg = btn_sport.getText().toString();
                Log.d(TAG, "string gumba " + msg);
                if(msg.equals("start")){
                    //spremeni v stop
                    Log.d(TAG, "sem notri v PRVI IF");
                    msg = getString(R.string.stopwatch_stop);
                    Intent intent = new Intent(StopwatchActivity.this, TrackerService.class);
                    intent.setAction("si.uni_lj.fri.pbd2019.runsup.COMMAND_START");
                    startService(intent);

                } else if(msg.equals("stop")){
                    //spremeni v contiune in prikazi drugi gumb
                    Log.d(TAG, "sem notri v DRUGI IF");
                    msg = getString(R.string.stopwatch_continue);
                    btn_end.setVisibility(View.VISIBLE);
                    Intent intent = new Intent(StopwatchActivity.this, TrackerService.class);
                    intent.setAction("si.uni_lj.fri.pbd2019.runsup.COMMAND_PAUSE");
                    startService(intent);
                    postitionLists.add(positionList);
                    Log.d(TAG, "Dolzina lista dolzin: " + postitionLists.size());

                }else if(msg.equals("continue")){
                    //spremeni v stop in odstani drugi gumb
                    Log.d(TAG, "sem notri v TRETJI IF");
                    msg = getString(R.string.stopwatch_stop);
                    btn_end.setVisibility(View.GONE);
                    Intent intent = new Intent(StopwatchActivity.this, TrackerService.class);
                    intent.setAction("si.uni_lj.fri.pbd2019.runsup.COMMAND_CONTINUE");
                    startService(intent);

                }
                btn_sport.setText(msg);

            }
        });

    }

    //Funkcije za location permision
    public void askPermission() {
        Snackbar.make(findViewById(R.id.constraintlayout_stopwatch_content), "This app needs to know your location, ask for permission",
                Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // If the user agrees with the Snackbar, proceed with asking for the permissions:
                        ActivityCompat.requestPermissions(StopwatchActivity.this,
                                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                                        Manifest.permission.ACCESS_FINE_LOCATION},
                                REQUEST_ID_LOCATION_PERMISSIONS);
                    }
                }).show();


    }

    //-----------------------------------------------

    private BroadcastReceiver mRecevier = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "BROADCAST RECIVED");
            Bundle extras = intent.getExtras();

            //DOBIŠ TICK IN UPDEJTAJ
            updateUI(extras);

        }
    };

    private void updateUI(Bundle extras){
        extras.getInt("state");
        duration = extras.getLong("duration");
        distance = extras.getDouble("distance");
        pace = extras.getDouble("pace");
        state = extras.getInt("state", 0);
        avgSpeed = extras.getDouble("avgSpeed");

        if(pace > 1000.0){
            pace = 0.00;
        }
        calories = extras.getDouble("calories");
        positionList = (ArrayList<Location>) extras.getSerializable("positionList");
        //manjka se uni shit za google

        //PRETVORI V STRING
        String durationString = mainHelper.timeChange(duration / 1000);
        String distanceString = mainHelper.formatDistance(distance);
        String paceString = mainHelper.formatPace(pace);
        String caloriesString = String.valueOf((int)Math.floor(calories));
        /*
            mainHelper.timeChange(mDuration / 1000)
            mainHelper.formatDistance(mDistance)
            mainHelper.formatPace(mPace)
         */

        //POSODOBI DURATION
        TextView duration = (TextView) findViewById(R.id.textview_stopwatch_duration);
        duration.setText(durationString);

        //POSODOBI DISTANCE
        TextView distanceView = (TextView) findViewById(R.id.textview_stopwatch_distance);
        distanceView.setText(distanceString);

        //POSODOBI PACE
        TextView paceView = (TextView) findViewById(R.id.textview_stopwatch_pace);
        paceView.setText(paceString);

        //POSODOBI PACE
        TextView calView = (TextView) findViewById(R.id.textview_stopwatch_calories);
        calView.setText(caloriesString);
    }


    //-------------------------------------------------
    //ALERT DIALOGS CODE

    public void alertCreate(){
        AlertDialog.Builder alertDisplay = new AlertDialog.Builder(StopwatchActivity.this);
        alertDisplay.setMessage("Do you really want to end wourkout and reset counters?").setCancelable(false)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        openWorkoutDetailActivity();
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = alertDisplay.create();
        alert.setTitle("End Actual Workout?");
        alert.show();
    }

    public void radioButtonAlert(){

        AlertDialog.Builder alertDisplay = new AlertDialog.Builder(StopwatchActivity.this);
        alertDisplay.setTitle("Select Sport Activity");
        sportType = 0;
        alertDisplay.setSingleChoiceItems(sportSelection, sportActivity, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case 0:
                        sportType = 0;
                        Log.d(TAG, "we are " +  sportSelection[which]);
                        break;
                    case 1:
                        Log.d(TAG, "we are " +  sportSelection[which]);
                        sportType = 1;
                        break;
                    case 2:
                        Log.d(TAG, "we are " +  sportSelection[which]);
                        sportType = 2;
                        break;
                }

            }
        });
        alertDisplay.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                //changed button text when user clicks OK
                sportActivity = sportType;
                btn_select_sport.setText(sportSelection[sportActivity]);

                //Started service intent
                Intent intent = new Intent(StopwatchActivity.this, TrackerService.class);
                intent.putExtra("sportActivity", sportActivity);
                //POSLAT MORAS SE WORKOUT ID

                intent.setAction("si.uni_lj.fri.pbd2019.runsup.UPDATE_SPORT_ACTIVITY");
                startService(intent);
            }
        });
        alertDisplay.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });


        AlertDialog alert = alertDisplay.create();
        alert.show();
    }

    //-------------------------------------------------

    public void openWorkoutDetailActivity() {

        Dao<Workout, Long> workoutDao = null;
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        try {
            workoutDao = databaseHelper.workoutDao();
        } catch (SQLException e){
            e.printStackTrace();
        }

        Workout workout = new Workout("Workout", 0);
        try {
            workoutDao.create(workout);

        } catch (SQLException e){
            e.printStackTrace();
        }
        List<Workout> workouts = null;
        try{
            QueryBuilder<Workout, Long> workoutBuilder = workoutDao.queryBuilder();
            Where where = workoutBuilder.where();
            where.eq(String.valueOf(Workout.statusUnknown), 0);
            workouts = workoutBuilder.query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String title = workouts.get(0).getTitle();
        Log.d(TAG, title);
        Log.d(TAG, "KRENI DA VIDIM CE DELA");
        Bundle extras = new Bundle();

        extras.putInt("sportActivity", sportActivity);
        extras.putLong("duration", duration);
        extras.putDouble("distance", distance);
        //extras.putDouble("pace", pace);
        extras.putDouble("calories", calories);
        extras.putDouble("pace", avgSpeed);
        extras.putSerializable("finalPositionList", postitionLists);
        //MANJKA SE LOCATION LIST EXTRA!!
        Log.d(TAG, "DURATION SENT: " + duration);
        Intent intent = new Intent(this, WorkoutDetailActivity.class);
        intent.putExtras(extras);

        stopService(new Intent(StopwatchActivity.this, TrackerService.class));

        startActivity(intent);


    }

    @Override
    public void onPause() {
        unregisterReceiver(mRecevier);
        if(state == 0){
            Intent intent = new Intent(StopwatchActivity.this, TrackerService.class);
            stopService(intent);
        }
        super.onPause();
    }

    @Override
    protected void onResume() {
        registerReceiver(mRecevier, mfilter);
        //to si dodau

        Intent intent = new Intent(StopwatchActivity.this, TrackerService.class);
        startService(intent);
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
