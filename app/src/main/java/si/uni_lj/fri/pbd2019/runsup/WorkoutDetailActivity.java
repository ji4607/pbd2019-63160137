package si.uni_lj.fri.pbd2019.runsup;

import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import org.w3c.dom.Text;

import java.io.Serializable;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.fragments.AboutFragment;
import si.uni_lj.fri.pbd2019.runsup.fragments.HistoryFragment;
import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;
import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;
import si.uni_lj.fri.pbd2019.runsup.settings.SettingsActivity;


public class WorkoutDetailActivity extends AppCompatActivity implements DialogWorkoutTitle.DialogListener, OnMapReadyCallback {

    private static final String TAG = "WorkoutDetail";

    //SPREMENLJIVKE
    private Button btn_map, btn_fb, btn_tw, btn_gm, btn_gp;
    private TextView workoutTitle_text, sportActivity_text, activityDate_text, duration_text, cal_text, distance_text, pace_text;
    private Long workoutId;
    private GoogleMap mMapActive;
    private ArrayList<List<Location>> finalPositionList;
    MainHelper mainHelper = new MainHelper();
    String message;
    private MarkerOptions start, end;

    private List<GpsPoint> locationList;
    //Dao objekti za bazo
    Dao<Workout, Long> workoutDao;
    Dao<GpsPoint, Long> gpsPointsDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workout_detail);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment_workoutdetail_map);
        mapFragment.getMapAsync(this);

        finalPositionList = new ArrayList<List<Location>>();
        locationList = new ArrayList<GpsPoint>();

        //INICIALIZIRAJ DAO
        workoutDao = null;
        gpsPointsDao = null;
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(WorkoutDetailActivity.this, DatabaseHelper.class);
        try {
            gpsPointsDao = databaseHelper.gpsPointDao();
            workoutDao = databaseHelper.workoutDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        //PODATKI IZ STOPWATCH ACTICITY
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        Log.d(TAG, "before setting extras.");
        if (extras != null) {
            workoutId = extras.getLong("workoutId");
            finalPositionList = (ArrayList<List<Location>>) extras.getSerializable("finalPositionList");
        }
        Log.d(TAG, "WORKOUT ID: " + workoutId.toString());

        //INICIALIZIRAJ VSE TEXT VIEWS
        workoutTitle_text = (TextView) findViewById(R.id.textview_workoutdetail_workouttitle);
        sportActivity_text = (TextView) findViewById(R.id.textview_workoutdetail_sportactivity);
        activityDate_text = (TextView) findViewById(R.id.textview_workoutdetail_activitydate);
        duration_text = (TextView) findViewById(R.id.textview_workoutdetail_valueduration);
        cal_text = (TextView) findViewById(R.id.textview_workoutdetail_valuecalories);
        distance_text = (TextView) findViewById(R.id.textview_workoutdetail_valuedistance);
        pace_text = (TextView) findViewById(R.id.textview_workoutdetail_valueavgpace);

        //showDisplayCorrectly();
        getGpsPoints();
        updateUI();
        createMessage();

        btn_map = (Button) findViewById(R.id.button_workoutdetail_showmap);
        btn_fb = (Button) findViewById(R.id.button_workoutdetail_fbsharebtn);
        btn_tw = (Button) findViewById(R.id.button_workoutdetail_twittershare);
        btn_gm = (Button) findViewById(R.id.button_workoutdetail_emailshare);
        btn_gp = (Button) findViewById(R.id.button_workoutdetail_gplusshare);

        btn_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMapsActivity();
            }
        });

        btn_fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initShareIntentFb();
            }
        });

        btn_gm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initShareIntentMail();
            }
        });


        btn_tw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //initShareIntentTwitter();
                shareTwitter();
            }
        });

        workoutTitle_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog();
            }
        });


    }

    //ZA OPTION MENU DESNO ZGORAJ
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.workout_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(WorkoutDetailActivity.this, SettingsActivity.class);
            startActivity(intent);

        }else if (id == R.id.action_delete_workout){
            try {
                Workout workout = workoutDao.queryForId(workoutId);
                workoutDao.delete(workout);
                finish();
                Intent intent = new Intent(WorkoutDetailActivity.this, MainActivity.class);
                startActivity(intent);

            } catch (SQLException e){
                e.printStackTrace();
            }

        }
        return super.onOptionsItemSelected(item);
    }




    public void openDialog(){
        DialogWorkoutTitle mDialog = new DialogWorkoutTitle();
        mDialog.show(getSupportFragmentManager(), "DialogWorkoutTitle");
    }


    public void openMapsActivity() {
        Intent intent = new Intent(this, MapsActivity.class);
        Bundle extras = new Bundle();
        extras.putLong("workoutId", workoutId);
        intent.putExtras(extras);
        startActivity(intent);
    }

   /* private void showDisplayCorrectly() {
        //SPORT ACTIVITY

        if (sportActivity == 0) {
            sportActivity_text.setText("Running");
        } else if (sportActivity == 1) {
            sportActivity_text.setText("Walking");
        } else {
            sportActivity_text.setText("Cycling");
        }

        //CURRENT DATE
        DateFormat dateFormat = SimpleDateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM);
        Date date = new Date();
        String strDate = dateFormat.format(date);
        activityDate_text.setText(strDate);


        String durationString = mainHelper.timeChange(duration / 1000);
        String distanceString = mainHelper.formatDistance(distance);
        String paceString = mainHelper.formatPace(avgSpeed);
        String caloriesString = String.valueOf((int) Math.floor(calories));

        //DURATION
        duration_text.setText(durationString);

        //CALORIES
        cal_text.setText(caloriesString + " kcal");

        //DISTANCE
        distance_text.setText(distanceString + " km");

        //PACE
        pace_text.setText(paceString + " min/km");
    }*/

    public void createMessage(){
        Workout workout;
        try {
            workout = workoutDao.queryForId(workoutId);
            String workoutType;
            if (workout.getSportActivity() == 0) {
                workoutType = "Running";
            } else if (workout.getSportActivity() == 1) {
                workoutType = "Walking";
            } else {
                workoutType = "Cycling";
            }
            message = "I was out for " + workoutType + ". I did " + mainHelper.formatDistance(workout.getDistance()) + " km in " + mainHelper.timeChange(workout.getDuration() / 1000) + " seconds.";
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void updateUI() {
        Log.d(TAG, "SEM NOTRI V UPDATE UI");
        //CURRENT DATE
        DateFormat dateFormat = SimpleDateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM);
        Date date = new Date();
        String strDate = dateFormat.format(date);
        activityDate_text.setText(strDate);

        Workout workout;
        try {
            workout =  workoutDao.queryForId(workoutId);

            //WORKOUT TITLE
            workoutTitle_text.setText(workout.getTitle());

            //SPORT ACTIVITY
            if (workout.getSportActivity() == 0) {
                sportActivity_text.setText("Running");
            } else if (workout.getSportActivity() == 1) {
                sportActivity_text.setText("Walking");
            } else if(workout.getSportActivity() == 2){
                sportActivity_text.setText("Cycling");
            }

            //DURATION
            String durationString = mainHelper.timeChange(workout.getDuration() / 1000);
            duration_text.setText(durationString);

            //CALORIES
            String caloriesString = String.valueOf((int) Math.floor(workout.getTotalCalories()));
            cal_text.setText(caloriesString + " kcal");

            //DISTANCE
            String distanceString = mainHelper.formatDistance(workout.getDistance());
            distance_text.setText(distanceString + " km");

            //PACE
            String paceString = mainHelper.formatPace(workout.getPaceAvg());
            pace_text.setText(paceString + " min/km");


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }



    public void shareTwitter() {
        try {
            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
            sharingIntent.setClassName("com.twitter.android", "com.twitter.android.PostActivity");
            sharingIntent.putExtra(Intent.EXTRA_TEXT, message);
            startActivity(sharingIntent);
        } catch (Exception e) {
            Log.e("In Exception", "Comes here");
            Intent i = new Intent();
            i.putExtra(Intent.EXTRA_TEXT, message);
            i.setAction(Intent.ACTION_VIEW);
            i.setData(Uri.parse("https://mobile.twitter.com/compose/tweet"));
            startActivity(i);
        }
    }

    private void initShareIntentFb() {
        boolean found = false;
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");

        // gets the list of intents that can be loaded.
        List<ResolveInfo> resInfo = getPackageManager().queryIntentActivities(share, 0);
        if (!resInfo.isEmpty()) {
            for (ResolveInfo info : resInfo) {
                Log.d(TAG, "package name: " + info.activityInfo.packageName.toLowerCase());
                if (info.activityInfo.packageName.toLowerCase().contains("facebook") ||
                        info.activityInfo.name.toLowerCase().contains("facebook")) {
                    Log.d(TAG, "TE SE DODAJO V PAKET: " + info.activityInfo.packageName.toLowerCase());
                    share.putExtra(Intent.EXTRA_TEXT, message);
                    share.setPackage(info.activityInfo.packageName);
                    found = true;
                }
            }
            if (!found)
                return;

            startActivity(Intent.createChooser(share, "Select"));
        }
    }

    private void initShareIntentMail() {
        boolean found = false;
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");

        // gets the list of intents that can be loaded.
        List<ResolveInfo> resInfo = getPackageManager().queryIntentActivities(share, 0);
        if (!resInfo.isEmpty()) {
            for (ResolveInfo info : resInfo) {
                Log.d(TAG, "package name: " + info.activityInfo.packageName.toLowerCase());
                if (info.activityInfo.packageName.toLowerCase().contains("gm") ||
                        info.activityInfo.name.toLowerCase().contains("gm")) {
                    Log.d(TAG, "TE SE DODAJO V PAKET: " + info.activityInfo.packageName.toLowerCase());
                    share.putExtra(Intent.EXTRA_TEXT, message);
                    share.setPackage(info.activityInfo.packageName);
                    found = true;
                }
            }
            if (!found)
                return;

            startActivity(Intent.createChooser(share, "Select"));
        }
    }



    public void getGpsPoints(){
        try{
            List<GpsPoint> gpsPointList = gpsPointsDao.queryForAll();

            for(GpsPoint x : gpsPointList){

                Long id = x.getWorkout().getId();
                if(id.equals(workoutId)){
                    locationList.add(x);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void setMarkers(){
        GpsPoint startPoint;
        GpsPoint endPoint;
        if(locationList.size() != 0){
            if(locationList.size() > 1){
                startPoint = locationList.get(0);
                endPoint = locationList.get(locationList.size()-1);

                LatLng startLatLng = new LatLng(startPoint.getLatitude(),
                        startPoint.getLongitude());

                LatLng endLatLng = new LatLng(endPoint.getLatitude(),
                        endPoint.getLongitude());

                start = new MarkerOptions().position(startLatLng).title("START");
                end = new MarkerOptions().position(endLatLng).title("END");


                mMapActive.addMarker(end).showInfoWindow();
                mMapActive.addMarker(start).showInfoWindow();

                CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(startLatLng, 18);
                mMapActive.animateCamera(yourLocation);

            }else{

                startPoint = locationList.get(0);
                LatLng startLatLng = new LatLng(startPoint.getLatitude(),
                        startPoint.getLongitude());

                start = new MarkerOptions().position(startLatLng).title("START");
                mMapActive.addMarker(start).showInfoWindow();
                CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(startLatLng, 18);
                mMapActive.animateCamera(yourLocation);

            }
        }

    }



    @Override
    public void onPause() {
        super.onPause();
    }


    @Override
    public void applyText(String title) {
        Workout workout = new Workout();
        try {
            workout = workoutDao.queryForId(workoutId);
            workout.setTitle(title);
            workoutDao.createOrUpdate(workout);

        } catch (SQLException e){
            e.printStackTrace();
        }
        workoutTitle_text.setText(workout.getTitle());
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMapActive = googleMap;
        setMarkers();
    }
}
