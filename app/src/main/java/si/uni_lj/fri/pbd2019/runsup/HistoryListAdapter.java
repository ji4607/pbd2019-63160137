package si.uni_lj.fri.pbd2019.runsup;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.fragments.HistoryFragment;
import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;

public class HistoryListAdapter extends ArrayAdapter<Workout> {


    /*public HistoryListAdapter(@NonNull Context context, int resource) {
        super(context, resource);
    }*/
    private Long workoutID;
    MainHelper mainHelper = new MainHelper();

    public HistoryListAdapter(@NonNull Context context, List<Workout> workouts) {
        super(context, 0, workouts);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Workout workout = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.adapter_history, parent, false);
        }
        workoutID = workout.getId();

        // Lookup view for data population
        ImageView img = (ImageView) convertView.findViewById(R.id.imageview_history_icon);
        TextView title = (TextView) convertView.findViewById(R.id.textview_history_title);
        TextView date = (TextView) convertView.findViewById(R.id.textview_history_datetime);
        TextView info_text = (TextView) convertView.findViewById(R.id.textview_history_sportactivity);
        // Populate the data into the template view using the data object

        title.setText(workout.getTitle());
        //img.setImageResource(R.drawable.ic_facebook_logo);
        //CURRENT DATE
        DateFormat dateFormat = SimpleDateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM);
        String strDate = dateFormat.format(workout.getCreated());
        date.setText(strDate);

        info_text.setText(setInfo(workout));
        convertView.setTag(position);
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = (Integer) v.getTag();
                Workout workout = getItem(position);
                workoutID = workout.getId();
                Intent intent = new Intent(getContext(), WorkoutDetailActivity.class);
                Bundle extras = new Bundle();
                extras.putLong("workoutId", workoutID);
                intent.putExtras(extras);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getContext().startActivity(intent);
            }
        });
        // Return the completed view to render on screen
        return convertView;
    }

    public String setInfo(Workout workout){

        String sportActivity = "";
        String labelDistanceKm = getContext().getString(R.string.all_labeldistanceunitkilometers);
        String labelCal = getContext().getString(R.string.all_labelcaloriesunit);
        String labelMin = getContext().getString(R.string.all_min);

        if (workout.getSportActivity() == 0) {
            sportActivity = "Running";
        } else if (workout.getSportActivity() == 1) {
            sportActivity = "Walking";
        } else if(workout.getSportActivity() == 2){
            sportActivity = "Cycling";
        }
        String durationString = mainHelper.timeChange(workout.getDuration() / 1000);
        String distanceString = mainHelper.formatDistance(workout.getDistance());
        String caloriesString = String.valueOf((int) Math.floor(workout.getTotalCalories()));
        String paceString = mainHelper.formatPace(workout.getPaceAvg());

        return (durationString + " " + sportActivity + " | " + distanceString + " " +  labelDistanceKm + " | " + caloriesString + " " + labelCal  + " | avg " + paceString + " " + labelMin + "/" + labelDistanceKm);

    }


}
