package si.uni_lj.fri.pbd2019.runsup.helpers;

public final class MainHelper {

    private static final float MpS_TO_MIpH = 2.23694f;
    private static final float KM_TO_MI = 0.62137119223734f;
    private static final float MINpKM_TO_MINpMI = 1.609344f;

    //return string of time in format HH:MM:SS
    public static String timeChange(long time){

        long hours = time / 3600;
        long minutes = (time % 3600) / 60;
        long seconds = time % 60;

        String timeToString = String.format("%02d:%02d:%02d", hours, minutes, seconds);

        return timeToString;
    }

    //convert m to km and round to 2 decimal places and return as string
    public static String formatDistance(double n){

        double roundKm = n * 100;
        double km = (double)((int) roundKm/1000) /100;

        String kmToString = String.format("%.2f", km);

        return kmToString;
    }

    //round number to 2 decimal places and return as string
    public static String formatPace(double n){

        double roundNumber = n * 100;
        double number = (double) ((int)roundNumber) /100;

        String numberToString = String.format("%.2f", number);

        return numberToString;
    }

    //round number to integer
    public static String formatCalories(double n){

        int intNumber = (int) n;

        return String.valueOf(intNumber);
    }

    //convert km to mi (multiply with a corresponding constant)
    public static double kmToMi(double n){
        return (n * KM_TO_MI);
    }

    //convert m/s to mi/h (multiply with a corresponding constant)
    public static double mpsToMiph(double n){
        return (n * MpS_TO_MIpH);
    }

    //convert min/km to min/mi (multiply with a corresponding constant)
    public static double minpkmToMinpmi(double n){
        return (n * MINpKM_TO_MINpMI);
    }

}
