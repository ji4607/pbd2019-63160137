package si.uni_lj.fri.pbd2019.runsup;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;

public class CompeteListAdapter extends ArrayAdapter<Workout> {

    private Long workoutID;
    MainHelper mainHelper = new MainHelper();
    //Dao objekti za bazo
    Dao<Workout, Long> workoutDao;
    private SharedPreferences mSharedPref;
    private SharedPreferences.Editor mEditor;

    private static final int NOT_SELECTED = -1;
    private int selectedPos = NOT_SELECTED;

    // if called with the same position multiple lines it works as toggle
    public void setSelection(int position) {
        if (selectedPos == position) {
            selectedPos = NOT_SELECTED;
        } else {
            selectedPos = position;
        }
        notifyDataSetChanged();
    }

    public CompeteListAdapter(@NonNull Context context, List<Workout> workouts) {

        super(context, 0, workouts);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        mSharedPref = getContext().getSharedPreferences("SharedPref", Context.MODE_PRIVATE);
        mEditor = mSharedPref.edit();
        // Get the data item for this position
        final Workout workout = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.adapter_compete, parent, false);
        }
        workoutID = workout.getId();

        // Lookup view for data population
        final TextView title = (TextView) convertView.findViewById(R.id.compete_w_title);
        TextView date = (TextView) convertView.findViewById(R.id.compete_w_date);
        TextView info_text = (TextView) convertView.findViewById(R.id.compete_w_info);
        // Populate the data into the template view using the data object

        title.setText(workout.getTitle());
        //img.setImageResource(R.drawable.ic_facebook_logo);
        //CURRENT DATE
        DateFormat dateFormat = SimpleDateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM);
        String strDate = dateFormat.format(workout.getCreated());
        date.setText(strDate);

        info_text.setText(setInfo(workout));
        if (position == selectedPos) {
            // your color for selected item
            convertView.setBackgroundColor(convertView.getResources().getColor(R.color.colorPrimary));
            Workout selectedWorkout = getItem(position);
            workoutID = selectedWorkout.getId();
            mEditor.putLong("workoutId", workoutID);
            mEditor.commit();


        } else {
            // your color for non-selected item
            convertView.setBackgroundColor(convertView.getResources().getColor(R.color.colorWhite));
        }
        /*convertView.setTag(position);
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = (Integer) v.getTag();
                Workout workout = getItem(position);
                workoutID = workout.getId();
                mEditor.putLong("workoutId", workoutID);
                mEditor.commit();
            }
        });*/
        // Return the completed view to render on screen


        return convertView;
    }

    public String setInfo(Workout workout){

        String sportActivity = "";
        String labelDistanceKm = getContext().getString(R.string.all_labeldistanceunitkilometers);
        String labelCal = getContext().getString(R.string.all_labelcaloriesunit);
        String labelMin = getContext().getString(R.string.all_min);

        if (workout.getSportActivity() == 0) {
            sportActivity = "Running";
        } else if (workout.getSportActivity() == 1) {
            sportActivity = "Walking";
        } else if(workout.getSportActivity() == 2){
            sportActivity = "Cycling";
        }
        String durationString = mainHelper.timeChange(workout.getDuration() / 1000);
        String distanceString = mainHelper.formatDistance(workout.getDistance());
        String caloriesString = String.valueOf((int) Math.floor(workout.getTotalCalories()));
        String paceString = mainHelper.formatPace(workout.getPaceAvg());

        return (durationString + " " + sportActivity + " | " + distanceString + " " +  labelDistanceKm + " | " + caloriesString + " " + labelCal  + " | avg " + paceString + " " + labelMin + "/" + labelDistanceKm);

    }


}
