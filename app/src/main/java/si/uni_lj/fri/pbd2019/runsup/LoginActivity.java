package si.uni_lj.fri.pbd2019.runsup;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;

import org.w3c.dom.Text;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.User;
import si.uni_lj.fri.pbd2019.runsup.model.UserProfile;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;
import si.uni_lj.fri.pbd2019.runsup.services.TrackerService;
import si.uni_lj.fri.pbd2019.runsup.settings.SettingsActivity;

public class LoginActivity extends AppCompatActivity implements Dialog.DialogListener {

    private static final String TAG = "LoginActivity";
    private static final int RC_SIGN_IN = 1;

    private LinearLayout mWeightLayout;
    private TextView mWeight;
    private ImageView mProfilePic;
    private TextView mProfileName;
    private SignInButton btn_sign_in;
    private Button btn_sign_out;
    private FirebaseAuth mAuth;
    GoogleSignInClient mGoogleSignInClient;
    GoogleApiClient mGoogleApiClient;
    private SharedPreferences mSharedPref;
    private SharedPreferences.Editor mEditor;

    //Dao objekti za bazo
    private Dao<User, Long> userDao;
    private Dao<UserProfile, Long> userProfileDao;
    private DatabaseHelper databaseHelper;
    private Long userId;

    private boolean userSignedIn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // ...
        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();

        //INICIALIZIRAJ DAO
        userDao = null;
        userProfileDao = null;
        databaseHelper = OpenHelperManager.getHelper(LoginActivity.this, DatabaseHelper.class);
        try {
            userDao = databaseHelper.userDao();
            userProfileDao = databaseHelper.userProfileDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        userId = null;

        userSignedIn = false;
        mSharedPref = getSharedPreferences("SharedPref", Context.MODE_PRIVATE);
        mEditor = mSharedPref.edit();

        btn_sign_in = (SignInButton) findViewById(R.id.sign_in_button);
        btn_sign_out = (Button) findViewById(R.id.sign_out_button);

        mWeightLayout = (LinearLayout) findViewById(R.id.weight_display_layout);
        mWeight = (TextView) findViewById(R.id.weight_number);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        btn_sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });

        btn_sign_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signOut();
            }
        });

        mWeightLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog();
            }
        });

    }

    public void openDialog() {
        Dialog mDialog = new Dialog();
        mDialog.show(getSupportFragmentManager(), "Dialog");
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
    }


    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e);
                // ...
            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            userSignedIn = true;
                            createUserDb(user);
                            updateUI(user);
                            Log.d(TAG, "Login SUccesful");

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            updateUI(null);
                        }

                        // ...
                    }
                });
    }

    public void createUserDb(FirebaseUser user) {
        GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(getApplicationContext());


        User userdb = new User();
        UserProfile userProfile = new UserProfile();
        List<User> userList = new ArrayList<User>();

        if (acct != null) {
            //Ustvarimo uporabinka in njegov profil v bazi če še ne obstaja
            String tokenId = acct.getIdToken();
            String accId = acct.getId();


            // get our query builder from the DAO
            QueryBuilder<User, Long> queryBuilder =
                    userDao.queryBuilder();
            try{
                // the 'password' field must be equal to "qwerty"
                queryBuilder.where().eq(User.ACCID_FIELD_NAME, accId);
                // prepare our sql statement
                PreparedQuery<User> preparedQuery = queryBuilder.prepare();
                // query for all accounts that have "qwerty" as a password
                userList = userDao.query(preparedQuery);
            } catch (SQLException e){
                e.printStackTrace();
            }

            if(userList.size() == 0){
                try{
                    userDao.createIfNotExists(userdb);

                    userdb.setAccId(accId);
                    userdb.setAuthToken(tokenId);
                    userDao.update(userdb);

                    userProfile.setUser(userdb);
                    userProfile.setWeight(60.0);
                    userProfileDao.createIfNotExists(userProfile);
                    Log.d(TAG, "ustvarjeni v bazi");

                }catch (SQLException e){
                    e.printStackTrace();
                }
            }else{
                Log.d(TAG, "dolzina lista " + userList.size());
                userdb = userList.get(0);
                Double weightUser = 60.0;
                try {
                    List<UserProfile> userProfileList = userProfileDao.queryForAll();
                    for(UserProfile up : userProfileList){
                        if(up.getUser().getId().equals(userdb.getId())){
                            weightUser = up.getWeight();
                            break;
                        }
                    }
                } catch (SQLException e){
                    e.printStackTrace();
                }
                String weight = String.valueOf(weightUser);
                Log.d(TAG, weight);
                mEditor.putString("weight", weight);
                mEditor.commit();
            }
        }

    }

    private void updateUI(FirebaseUser user) {

        GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(getApplicationContext());

        if (acct != null) {
            String personName = acct.getDisplayName();
            String personEmail = acct.getEmail();
            Uri imageUri = acct.getPhotoUrl();


            btn_sign_out = (Button) findViewById(R.id.sign_out_button);
            btn_sign_out.setVisibility(View.VISIBLE);
            //mProfileName.setText(personName);

            mEditor.putBoolean("userSignedIn", userSignedIn);
            mEditor.putString("username", personName);
            mEditor.putString("url", imageUri.toString());

            mEditor.commit();
            Log.d(TAG, "person name: " + personName);

            Toast.makeText(this, "Name of user is " + personName, Toast.LENGTH_SHORT).show();
        }
    }

    private void signOut() {
        btn_sign_out = (Button) findViewById(R.id.sign_out_button);
        btn_sign_out.setVisibility(View.GONE);
        FirebaseAuth.getInstance().signOut();
        // Google sign out
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        // ...
                        userSignedIn = false;
                        mEditor.putBoolean("userSignedIn", userSignedIn);
                        mEditor.commit();
                    }
                });

    }

    @Override
    public void applyText(String weight) {
        //preveri če je še kdo prijauljen into, in njemu tudi zamenjaj


        //double d = new Double(weight).doubleValue();
        mEditor.putString("weight", weight);
        mEditor.commit();
        //Log.d(TAG, "double uspeu" + d);
        mWeight.setText(weight + " kg");
    }
}
