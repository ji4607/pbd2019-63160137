package si.uni_lj.fri.pbd2019.runsup.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.HistoryListAdapter;
import si.uni_lj.fri.pbd2019.runsup.R;
import si.uni_lj.fri.pbd2019.runsup.WorkoutDetailActivity;
import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.User;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;
import si.uni_lj.fri.pbd2019.runsup.settings.SettingsActivity;

public class HistoryFragment extends Fragment {

    private static View v;
    private TextView noHistory;

    private static MenuItem history;
    private static MenuItem delete;

    //Dao objekti za bazo
    Dao<Workout, Long> workoutDao;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_history, container, false);
        setHasOptionsMenu(true);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);



        //INICIALIZIRAJ DAO
        workoutDao = null;
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(getActivity(), DatabaseHelper.class);
        try {
            workoutDao = databaseHelper.workoutDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        noHistory = (TextView) v.findViewById(R.id.textview_history_noHistoryData);

        // Construct the data source
        ArrayList<Workout> workouts = new ArrayList<Workout>();
        List<Workout> workoutList = new ArrayList<Workout>();
        QueryBuilder<Workout, Long> queryBuilder =
                workoutDao.queryBuilder();
        try{
            // the 'password' field must be equal to "qwerty"
            queryBuilder.where().eq(Workout.STATUS_FIELD_NAME, Workout.statusEnded);
            // prepare our sql statement
            //queryBuilder.orderBy("created", false);
            PreparedQuery<Workout> preparedQuery = queryBuilder.prepare();
            // query for all accounts that have "qwerty" as a password
            workoutList = workoutDao.query(preparedQuery);
        } catch (SQLException e){
            e.printStackTrace();
        }
        /*try{
            workoutList = workoutDao.queryForAll();
        }catch (SQLException e){
            e.printStackTrace();
        }*/
        if(workoutList.size() != 0){
            // Create the adapter to convert the array to views
            HistoryListAdapter adapter = new HistoryListAdapter(getActivity(), workoutList);
            // Attach the adapter to a ListView
            ListView listView = (ListView) v.findViewById(R.id.listview_history_workouts);
            listView.setAdapter(adapter);
        }else{
            noHistory.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.history_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(getActivity(), SettingsActivity.class);
            startActivity(intent);

        }else if (id == R.id.button1){
            alertCreate();
        }else if (id == R.id.action_sync){

        }
        return super.onOptionsItemSelected(item);
    }

    public void alertCreate(){
        AlertDialog.Builder alertDisplay = new AlertDialog.Builder(getActivity());
        alertDisplay.setMessage("Are you sure you want to delete all of your history?").setCancelable(false)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        clearHistory();
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = alertDisplay.create();
        alert.setTitle("Clear history");
        alert.show();
    }

    //IZBRIŠI SMAMO UNE KI IMAJO STATUS 3?
    public void clearHistory(){
        try{
            //List<Workout> workoutList = workoutDao.queryForAll();

            // Construct the data source
            List<Workout> workoutList = new ArrayList<Workout>();
            QueryBuilder<Workout, Long> queryBuilder =
                    workoutDao.queryBuilder();
            try{
                // the 'password' field must be equal to "qwerty"
                queryBuilder.where().eq(Workout.STATUS_FIELD_NAME, Workout.statusEnded);
                // prepare our sql statement
                PreparedQuery<Workout> preparedQuery = queryBuilder.prepare();
                // query for all accounts that have "qwerty" as a password
                workoutList = workoutDao.query(preparedQuery);
            } catch (SQLException e){
                e.printStackTrace();
            }
            for(Workout x : workoutList){
                workoutDao.delete(x);
            }

            // Reload current fragment
            getFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, new HistoryFragment()).commit();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
