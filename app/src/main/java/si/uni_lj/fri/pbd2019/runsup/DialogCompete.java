package si.uni_lj.fri.pbd2019.runsup;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.Dialog;
import si.uni_lj.fri.pbd2019.runsup.R;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;

public class DialogCompete extends AppCompatDialogFragment {
    private DialogListener listener;

    private TextView noHistory;
    private SharedPreferences mSharedPref;
    private SharedPreferences.Editor mEditor;
    int flag = 0;
    //Dao objekti za bazo
    Dao<Workout, Long> workoutDao;

    @Override
    public android.app.Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_compete, null);

        //INICIALIZIRAJ DAO
        workoutDao = null;
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(getActivity(), DatabaseHelper.class);
        try {
            workoutDao = databaseHelper.workoutDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        noHistory = (TextView) view.findViewById(R.id.no_compete_data);

        // Construct the data source
        List<Workout> workoutList = new ArrayList<Workout>();
        QueryBuilder<Workout, Long> queryBuilder =
                workoutDao.queryBuilder();
        try{
            // the 'password' field must be equal to "qwerty"
            queryBuilder.where().eq(Workout.STATUS_FIELD_NAME, Workout.statusEnded);
            // prepare our sql statement
            queryBuilder.orderBy("created", false);
            PreparedQuery<Workout> preparedQuery = queryBuilder.prepare();
            // query for all accounts that have "qwerty" as a password
            workoutList = workoutDao.query(preparedQuery);
        } catch (SQLException e){
            e.printStackTrace();
        }

        if(workoutList.size() != 0){
            // Create the adapter to convert the array to views
            final CompeteListAdapter adapter = new CompeteListAdapter(getActivity(), workoutList);
            // Attach the adapter to a ListView
            ListView listView = (ListView) view.findViewById(R.id.listview_compete_workouts);
            listView.setAdapter(adapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    adapter.setSelection(position);

                }
            });
        }else{
            noHistory.setVisibility(View.VISIBLE);
        }

        builder.setView(view)
                .setTitle("Select your competitor")
                .setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setPositiveButton("Select", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        listener.applyText();
                    }
                });

        return builder.create();
    }

   @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            listener = (DialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() +
                    "must implement DialogListener");
        }
    }

    public interface DialogListener{
        void applyText();
    }
}
