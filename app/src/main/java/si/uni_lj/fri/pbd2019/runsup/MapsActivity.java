package si.uni_lj.fri.pbd2019.runsup;

import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private static final String TAG = "MapsActivity";

    private GoogleMap mMapActive;
    private List<GpsPoint> locationList;
    private MarkerOptions start, end;
    private Long workoutId;

    //Dao objekti za bazo
    Dao<Workout, Long> workoutDao;
    Dao<GpsPoint, Long> gpsPointsDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment_maps_workoutmap);
        mapFragment.getMapAsync(this);

        workoutId = null;

        //INICIALIZIRAJ DAO
        workoutDao = null;
        gpsPointsDao = null;
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(MapsActivity.this, DatabaseHelper.class);
        try {
            gpsPointsDao = databaseHelper.gpsPointDao();
            workoutDao = databaseHelper.workoutDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        locationList = new ArrayList<GpsPoint>();
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMapActive = googleMap;

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        workoutId = extras.getLong("workoutId");
        Log.d(TAG, "workout id " + workoutId);
        getGpsPoints();
        setMarkers();
        drawPath();

    }

    public void drawPath(){
        if(locationList != null){
            GpsPoint current = null;
            GpsPoint before = null;
            for(int i = 1; i < locationList.size(); i++){
                current = locationList.get(i);
                before = locationList.get(i-1);
                LatLng currentLatLng = new LatLng(current.getLatitude(),
                        current.getLongitude());
                LatLng beforeLatLng = new LatLng(before.getLatitude(),
                        before.getLongitude());

                mMapActive.addPolyline(new PolylineOptions().add(currentLatLng, beforeLatLng).width(10).color(Color.BLUE));

            }
        }
    }

    public void getGpsPoints(){
        try{
            List<GpsPoint> gpsPointList = gpsPointsDao.queryForAll();

            for(GpsPoint x : gpsPointList){
                Long id = x.getWorkout().getId();
                Log.d(TAG, "workout id "+ id);
                if(id.equals(workoutId)){
                    locationList.add(x);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void setMarkers(){
        GpsPoint startPoint;
        GpsPoint endPoint;
        if(locationList.size() != 0){
            if(locationList.size() > 1){
                startPoint = locationList.get(0);
                endPoint = locationList.get(locationList.size()-1);

                LatLng startLatLng = new LatLng(startPoint.getLatitude(),
                        startPoint.getLongitude());

                LatLng sendLatLng = new LatLng(endPoint.getLatitude(),
                        endPoint.getLongitude());

                start = new MarkerOptions().position(startLatLng).title("START");
                end = new MarkerOptions().position(sendLatLng).title("END");


                mMapActive.addMarker(end).showInfoWindow();
                mMapActive.addMarker(start).showInfoWindow();

                CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(startLatLng, 18);
                mMapActive.animateCamera(yourLocation);

            }else {

                startPoint = locationList.get(0);
                LatLng startLatLng = new LatLng(startPoint.getLatitude(),
                        startPoint.getLongitude());

                start = new MarkerOptions().position(startLatLng).title("START");
                mMapActive.addMarker(start).showInfoWindow();
                CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(startLatLng, 18);
                mMapActive.animateCamera(yourLocation);

            }
        }

    }
}
