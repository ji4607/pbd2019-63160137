package si.uni_lj.fri.pbd2019.runsup.fragments;


import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.ActiveWorkoutMapActivity;
import si.uni_lj.fri.pbd2019.runsup.R;
import si.uni_lj.fri.pbd2019.runsup.WorkoutDetailActivity;
import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;
import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;
import si.uni_lj.fri.pbd2019.runsup.services.TrackerService;
import si.uni_lj.fri.pbd2019.runsup.settings.SettingsActivity;

import static si.uni_lj.fri.pbd2019.runsup.services.TrackerService.REQUEST_ID_LOCATION_PERMISSIONS;
import android.support.v4.app.Fragment;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

public class StopwatchFragment extends Fragment {

    private static final String TAG = "StopFragment";
    private static ArrayList<List<Location>> postitionLists;

    MainHelper mainHelper = new MainHelper();

    //UI Komponente
    private Button btn_sport, btn_end, btn_active_map, btn_select_sport;
    private String[] sportSelection;

    //SPREMENLJIVKE
    private int sportActivity, state, sportType, voiceTimer, startedWIndex, startedLIndex, winningIndex, loosingIndex;
    private long duration;
    private double distance, pace, calories, avgSpeed;
    private static Location currentLocation;
    private static Location lastLocation;
    private Long workoutID;
    private SharedPreferences mSharedPref;
    private SharedPreferences.Editor mEditor;
    private Boolean competeMode, voiceMode;
    private Long competeWorkoutId;
    private double lastAvgPace, currentAvgPace, competingAcgPace, competingDistance;
    private long competingDuration;
    private ConstraintLayout constraintLayout;
    private Vibrator vibrator;
    private TextView compete_text, compete_pace;
    private String competitorPaceString, competitorDurationString, competitorDistanceString, endResult;
    //Dao objekti za bazo
    private Dao<Workout, Long> workoutDao;
    private Dao<GpsPoint, Long> gpsPointsDao;
    private DatabaseHelper databaseHelper;
    private ArrayList<MediaPlayer> startedWinning;
    private ArrayList<MediaPlayer> startedLoosing;
    private ArrayList<MediaPlayer> winning;
    private ArrayList<MediaPlayer> loosing;
    View v;
    private IntentFilter mfilter = new IntentFilter("si.uni_lj.fri.pbd2019.runsup.TICK");

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_stopwatch, container, false);
        setHasOptionsMenu(true);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //preveri če je premission usposobljen cene uprasi
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED) {
            askPermission();
        }
        vibrator = (Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE);
        mSharedPref = getContext().getSharedPreferences("SharedPref", Context.MODE_PRIVATE);
        mEditor = mSharedPref.edit();
        competeMode = false;
        voiceMode = false;
        compete_text = (TextView) v.findViewById(R.id.compete_text);
        compete_pace = (TextView) v.findViewById(R.id.compete_pace);
        voiceTimer = 0;
        //INICIALIZIRAJ DAO
        workoutDao = null;
        gpsPointsDao = null;
        startedWIndex = 0;
        startedLIndex = 0;
        winningIndex = 0;
        loosingIndex = 0;
        databaseHelper = OpenHelperManager.getHelper(getActivity(), DatabaseHelper.class);
        try {
            workoutDao = databaseHelper.workoutDao();
            gpsPointsDao = databaseHelper.gpsPointDao();
        }catch (SQLException e) {
            e.printStackTrace();
        }

        lastAvgPace = 0.0;
        currentAvgPace = 0.0;
        constraintLayout = (ConstraintLayout) v.findViewById(R.id.constraintlayout_stopwatch_content);


        workoutID = Long.valueOf(-1);
        state = 0;
        currentLocation = null;
        lastLocation = null;
        //postitionLists = new ArrayList<List<Location>>();
        avgSpeed = 0;
        sportActivity = 0;
        distance = 0;
        calories = 0;

        sportSelection = getResources().getStringArray(R.array.select_sport_array);
        btn_end = (Button) v.findViewById(R.id.button_stopwatch_endworkout);
        btn_sport = (Button) v.findViewById(R.id.button_stopwatch_start);
        btn_active_map = (Button) v.findViewById(R.id.button_stopwatch_activeworkout);
        btn_select_sport = (Button) v.findViewById(R.id.button_stopwatch_selectsport);

        Log.d(TAG, "SEM PRISU IZ ABOUT ACTIVITIJA");
        btn_select_sport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //testAlert();
                radioButtonAlert();
            }
        });

        btn_active_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //are you lost motherfucker??
                if(voiceMode){
                    MediaPlayer mp = MediaPlayer.create(getActivity(), R.raw.active_map);
                    mp.start();
                }

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        Intent intent = new Intent(getActivity(), ActiveWorkoutMapActivity.class);
                        startActivity(intent);
                    }
                }, 1500 );


            }
        });

        btn_end.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), TrackerService.class);
                intent.setAction("si.uni_lj.fri.pbd2019.runsup.COMMAND_PAUSE");
                getActivity().startService(intent);
                alertCreate();
            }
        });

        btn_sport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String msg = btn_sport.getText().toString();
                Log.d(TAG, "string gumba " + msg);
                if(msg.equals("start")){
                    //spremeni v stop
                    Log.d(TAG, "sem notri v PRVI IF");
                    msg = getString(R.string.stopwatch_stop);
                    Intent intent = new Intent(getActivity(), TrackerService.class);
                    intent.setAction("si.uni_lj.fri.pbd2019.runsup.COMMAND_START");
                    getActivity().startService(intent);

                } else if(msg.equals("stop")){
                    //spremeni v contiune in prikazi drugi gumb
                    Log.d(TAG, "sem notri v DRUGI IF");
                    msg = getString(R.string.stopwatch_continue);
                    btn_end.setVisibility(View.VISIBLE);
                    //UPDEJTAJ WORKOUT STATUS IN OSTALO POL ZA PRIKAZ!
                    updateOnStop();

                    Intent intent = new Intent(getActivity(), TrackerService.class);
                    intent.setAction("si.uni_lj.fri.pbd2019.runsup.COMMAND_PAUSE");
                    getActivity().startService(intent);
                    //postitionLists.add(positionList);
                    //Log.d(TAG, "Dolzina lista dolzin: " + postitionLists.size());

                }else if(msg.equals("continue")){
                    //spremeni v stop in odstani drugi gumb
                    Log.d(TAG, "sem notri v TRETJI IF");
                    try{
                        Workout workout = workoutDao.queryForId(workoutID);
                        workout.setStatus(Workout.statusUnknown);
                        workoutDao.createOrUpdate(workout);
                    } catch (SQLException e){
                        e.printStackTrace();
                    }

                    msg = getString(R.string.stopwatch_stop);
                    btn_end.setVisibility(View.GONE);
                    Intent intent = new Intent(getActivity(), TrackerService.class);
                    Bundle bundle = new Bundle();
                    bundle.putLong("workoutId", workoutID);
                    intent.putExtras(bundle);
                    intent.setAction("si.uni_lj.fri.pbd2019.runsup.COMMAND_CONTINUE");
                    getActivity().startService(intent);

                }
                btn_sport.setText(msg);

            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        // Construct the data source
        List<Workout> workoutList = new ArrayList<Workout>();
        QueryBuilder<Workout, Long> queryBuilder =
                workoutDao.queryBuilder();
        Where<Workout, Long> where = queryBuilder.where();
        try{
            where.or(where.eq(Workout.STATUS_FIELD_NAME, Workout.statusUnknown),
                    where.eq(Workout.STATUS_FIELD_NAME, Workout.statusPaused));

            PreparedQuery<Workout> preparedQuery = queryBuilder.prepare();

            workoutList = workoutDao.query(preparedQuery);
        } catch (SQLException e){
            e.printStackTrace();
        }

        //poledamo al ima status active al status paused
        if(workoutList.size() == 1){
            Workout workout = workoutList.get(0);
            workoutID = workout.getId();
            if(workout.getStatus() == 0){
                //ACTIVE WORKOUT
                btn_sport.setText(getString(R.string.stopwatch_stop));
            }else if(workout.getStatus() == 2) {
                //PAUSED WORKOUT
                workoutID = workout.getId();
                duration = workout.getDuration();
                distance = workout.getDistance();
                pace = workout.getPaceAvg();
                calories = workout.getTotalCalories();
                updateUI();
                btn_sport.setText(getString(R.string.stopwatch_continue));
                btn_end.setVisibility(View.VISIBLE);

            }

            sportActivity = workout.getSportActivity();
            btn_select_sport.setText(sportSelection[sportActivity]);

        } else if(workoutList.size() == 0){
            //RESETAJ FRAGMENT
            // Reload current fragment
            //getFragmentManager().beginTransaction()
                   // .replace(R.id.fragment_container, new StopwatchFragment()).commit();
            duration = 0;
            distance = 0;
            pace = 0;
            calories = 0;
            sportActivity = 0;
            btn_select_sport.setText(sportSelection[sportActivity]);
            btn_sport.setText(getString(R.string.stopwatch_start));
            btn_end.setVisibility(View.GONE);
            updateUI();
        }

        //pripravi tabele za voice
        startedWinning = new ArrayList<MediaPlayer>();
        startedWinning.add(MediaPlayer.create(getActivity(), R.raw.started_w1));
        startedWinning.add(MediaPlayer.create(getActivity(), R.raw.started_w2));

        startedLoosing = new ArrayList<MediaPlayer>();
        startedLoosing.add(MediaPlayer.create(getActivity(), R.raw.started_l1));
        startedLoosing.add(MediaPlayer.create(getActivity(), R.raw.started_l2));


        winning = new ArrayList<MediaPlayer>();
        winning.add(MediaPlayer.create(getActivity(), R.raw.winning_1));
        winning.add(MediaPlayer.create(getActivity(), R.raw.winning_2));


        loosing = new ArrayList<MediaPlayer>();
        loosing.add(MediaPlayer.create(getActivity(), R.raw.loosing_1));
        loosing.add(MediaPlayer.create(getActivity(), R.raw.loosing_2));

        compete_text.setVisibility(View.GONE);
        compete_pace.setVisibility(View.GONE);
        competeMode = mSharedPref.getBoolean("competeSwitch", false);
        voiceMode = mSharedPref.getBoolean("voiceSwitch", false);
        competeWorkoutId = mSharedPref.getLong("workoutId", -1);
        if(competeMode){
            if(competeWorkoutId == -1 || competeWorkoutId == null){
                Toast.makeText(getContext(), "Compete mode ON but no selected workout!", Toast.LENGTH_LONG).show();
                compete_text.setVisibility(View.GONE);
                compete_pace.setVisibility(View.GONE);
            }else{
                try{
                    Workout workout = workoutDao.queryForId(competeWorkoutId);
                    competingAcgPace = workout.getPaceAvg();
                    competingDuration = workout.getDuration();
                    competingDistance = workout.getDistance();

                    competitorPaceString = mainHelper.formatPace(competingAcgPace);
                    competitorDurationString = mainHelper.timeChange(competingDuration / 1000);
                    competitorDistanceString = mainHelper.formatDistance(competingDistance);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                Toast.makeText(getContext(), "Compete mode ON. Good luck!", Toast.LENGTH_LONG).show();
            }
        }else {
            compete_text.setVisibility(View.GONE);
            compete_pace.setVisibility(View.GONE);
        }



    }

    public void updateOnStop() {
        Workout workout = new Workout();

        try {
            workout = workoutDao.queryForId(workoutID);
            //zdej ga je potrebno se ustrezno dopolnit in updejtat
            workout.setDistance(distance);
            workout.setDuration(duration);
            workout.setTotalCalories(calories);
            workout.setPaceAvg(avgSpeed);
            workout.setSportActivity(sportActivity);
            workout.setStatus(Workout.statusPaused);
            workoutDao.createOrUpdate(workout);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //Funkcije za location permision
    public void askPermission() {
        Snackbar.make(v.findViewById(R.id.constraintlayout_stopwatch_content), "This app needs to know your location, ask for permission",
                Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // If the user agrees with the Snackbar, proceed with asking for the permissions:
                        ActivityCompat.requestPermissions(getActivity(),
                                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                                        Manifest.permission.ACCESS_FINE_LOCATION},
                                REQUEST_ID_LOCATION_PERMISSIONS);
                    }
                }).show();


    }

    //-----------------------------------------------

    private BroadcastReceiver mRecevier = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "BROADCAST RECIVED");
            Bundle extras = intent.getExtras();

            extras.getInt("state");
            duration = extras.getLong("duration");
            distance = extras.getDouble("distance");
            pace = extras.getDouble("pace");
            state = extras.getInt("state", 0);
            avgSpeed = extras.getDouble("avgSpeed", 0.0);
            workoutID = extras.getLong("workoutID");
            calories = extras.getDouble("calories");

            if(pace > 1000.0){
                pace = 0.00;
            }

            currentLocation = (Location) intent.getParcelableExtra("location");




            //DOBIŠ TICK IN UPDEJTAJ
            updateUI();
        }
    };

    private void updateUI(){


        //PRETVORI V STRING
        String durationString = mainHelper.timeChange(duration / 1000);
        String distanceString = mainHelper.formatDistance(distance);
        String paceString = mainHelper.formatPace(pace);
        String caloriesString = String.valueOf((int)Math.floor(calories));

        //POSODOBI DURATION
        TextView duration = (TextView) v.findViewById(R.id.textview_stopwatch_duration);
        duration.setText(durationString);

        //POSODOBI DISTANCE
        TextView distanceView = (TextView) v.findViewById(R.id.textview_stopwatch_distance);
        distanceView.setText(distanceString);

        //POSODOBI PACE
        TextView paceView = (TextView) v.findViewById(R.id.textview_stopwatch_pace);
        paceView.setText(paceString);

        //POSODOBI PACE
        TextView calView = (TextView) v.findViewById(R.id.textview_stopwatch_calories);
        calView.setText(caloriesString);

        //VSE TO IZVEDI SAMO CE JE COMPETITIVE MODE ON IN IMAMO SELECTAN WORKOUT
        if(competeMode && competeWorkoutId != -1 && competeWorkoutId != null) {
            if(voiceMode){
                voiceTimer += 1;
            }
            //preveri za čas če smo že tolko pretekli
            //preveri za kilometre če smo že tolko pretekli
            //spremeni barvo glede na avg pace
            if(Double.isNaN(avgSpeed)){
                avgSpeed = 0.0;
            }
            lastAvgPace = currentAvgPace;
            currentAvgPace = avgSpeed;
            Log.d(TAG, "WORKOUT AVG PACE: " + avgSpeed);
            Log.d(TAG, "LAST AVG PACE: " + lastAvgPace);
            Log.d(TAG, "CURRENT AVG PACE: " + currentAvgPace);
            Log.d(TAG, "COMPETITOR AVG PACE: " + competingAcgPace);

            if(currentAvgPace > competingAcgPace){
                endResult = "lost!";
                compete_text.setText("You are behind!");
                compete_pace.setText("Competitor avg. pace: " + competitorPaceString + " min/km");
                compete_pace.setTextColor(getResources().getColor(R.color.colorAccent));
                compete_text.setVisibility(View.VISIBLE);
                compete_pace.setVisibility(View.VISIBLE);
                compete_text.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                if(lastAvgPace >= competingAcgPace) {
                    //tu notri sem če zaostaja ze vec cajta
                    if(voiceMode && voiceTimer >= 25 && !(competitorDurationString.equals(durationString)) && !(competitorDistanceString.equals(distanceString))){
                        Log.d(TAG, "Loosing index: " + loosingIndex);
                        loosing.get(loosingIndex).start();
                        loosingIndex++;
                        if(loosingIndex > 1){
                            loosingIndex = 0;
                        }
                        voiceTimer = 0;
                    }
                    //common you gotta get back on track!
                    // ne zavibriraj in pusti rdeco barvo
                    //mogoče kej reči
                } else {
                    //tu notri sem če sem prej zmagovau in zdej zgubljam

                    vibrator.vibrate(300);
                    if(voiceMode && !(competitorDurationString.equals(durationString)) && !(competitorDistanceString.equals(distanceString))){
                        Log.d(TAG, "Started loosing: " + startedLIndex);
                        startedLoosing.get(startedLIndex).start();
                        startedLIndex++;
                        if(startedLIndex > 1){
                            startedLIndex = 0;
                        }
                    }
                    //zavibriraj
                    //kej reči
                }
            } else {
                endResult = "won!";
                compete_text.setText("You are ahead!");
                compete_pace.setText("Competitor avg. pace: " + competitorPaceString + " min/km");
                compete_pace.setTextColor(getResources().getColor(R.color.colorPrimary));
                compete_text.setVisibility(View.VISIBLE);
                compete_pace.setVisibility(View.VISIBLE);
                compete_text.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                if(lastAvgPace < competingAcgPace){
                    if(voiceMode && voiceTimer >= 25 && !(competitorDurationString.equals(durationString)) && !(competitorDistanceString.equals(distanceString))){
                        Log.d(TAG, "Winning index: " + winningIndex);
                        winning.get(winningIndex).start();
                        winningIndex++;
                        if(winningIndex > 1){
                            winningIndex = 0;
                        }
                        voiceTimer = 0;
                    }
                    //tu notri sem če sem jak in imam dober tempo  ze neki casa
                    //reci mu da naj keepa going into
                } else {
                    vibrator.vibrate(300);
                    if(voiceMode && !(competitorDurationString.equals(durationString)) && !(competitorDistanceString.equals(distanceString))){
                        Log.d(TAG, "Started winnin index: " + startedWIndex);
                        startedWinning.get(startedWIndex).start();
                        startedWIndex++;
                        if(startedWIndex > 1){
                            startedWIndex = 0;
                        }
                    }
                    //zavibriraj
                    //kej reči
                }
            }

            if(distanceString.equals(competitorDistanceString) || durationString.equals(competitorDurationString)){
                //ce je voice mode on, ustavi in reci da je workouta konec
                if(voiceMode){
                    //The competition is over
                    MediaPlayer mp = MediaPlayer.create(getActivity(), R.raw.end);
                    mp.start();
                }
                long[] pattern = {0, 300, 140, 300};
                vibrator.vibrate(pattern, -1);
                //ustavi workout in reci da je zmagau al zgubu
                //lahko zaključi workout al pa lahko nadaljuje
                competeMode = false;
                voiceMode = false;
                mEditor.putBoolean("voiceSwitch", false);
                mEditor.putBoolean("competeSwitch", false);
                mEditor.putLong("workoutId", -1);
                mEditor.commit();


                //ne pozabi spremenit shared activitija na compete mode off in workout id  -1
                btn_sport.setText(R.string.stopwatch_continue);
                btn_end.setVisibility(View.VISIBLE);
                //UPDEJTAJ WORKOUT STATUS IN OSTALO POL ZA PRIKAZ!
                updateOnStop();

                Intent intent = new Intent(getActivity(), TrackerService.class);
                intent.setAction("si.uni_lj.fri.pbd2019.runsup.COMMAND_PAUSE");
                getActivity().startService(intent);
                alertComepte();
                compete_text.setVisibility(View.GONE);
                compete_pace.setVisibility(View.GONE);
            }
        }


    }



    //-------------------------------------------------
    //ALERT DIALOGS CODE

    public void alertComepte(){
        AlertDialog.Builder alertDisplay = new AlertDialog.Builder(getActivity());
        alertDisplay.setMessage("Do you want to keep going or do you want to end workout?").setCancelable(false)
                .setPositiveButton(R.string.end, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        openWorkoutDetailActivity();
                    }
                })
                .setNeutralButton(R.string.keep_going, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = alertDisplay.create();
        alert.setTitle("Competition over, you " + endResult);
        alert.show();
    }

    public void alertCreate(){
        AlertDialog.Builder alertDisplay = new AlertDialog.Builder(getActivity());
        alertDisplay.setMessage("Do you really want to end wourkout and reset counters?").setCancelable(false)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        openWorkoutDetailActivity();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = alertDisplay.create();
        alert.setTitle("End Actual Workout?");
        alert.show();
    }

    public void radioButtonAlert(){

        AlertDialog.Builder alertDisplay = new AlertDialog.Builder(getActivity());
        alertDisplay.setTitle("Select Sport Activity");
        sportType = 0;
        alertDisplay.setSingleChoiceItems(sportSelection, sportActivity, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case 0:
                        sportType = 0;
                        Log.d(TAG, "we are " +  sportSelection[which]);
                        break;
                    case 1:
                        Log.d(TAG, "we are " +  sportSelection[which]);
                        sportType = 1;
                        break;
                    case 2:
                        Log.d(TAG, "we are " +  sportSelection[which]);
                        sportType = 2;
                        break;
                }

            }
        });
        alertDisplay.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                //changed button text when user clicks OK
                sportActivity = sportType;
                btn_select_sport.setText(sportSelection[sportActivity]);

                //Started service intent
                Intent intent = new Intent(getActivity(), TrackerService.class);
                Bundle bundle = new Bundle();
                bundle.putLong("workoutId", workoutID);
                intent.putExtras(bundle);
                intent.putExtra("sportActivity", sportActivity);

                try{
                    Workout workout = workoutDao.queryForId(workoutID);
                    if(workout != null){
                        String msg = btn_sport.getText().toString();
                        Log.d(TAG, "string gumba " + msg);

                        if(msg.equals("continue")){
                            workout.setStatus(Workout.statusPaused);

                        } else{
                            workout.setStatus(Workout.statusUnknown);
                        }
                        workoutDao.createOrUpdate(workout);
                    }

                } catch (SQLException e){
                    e.printStackTrace();
                }

                intent.setAction("si.uni_lj.fri.pbd2019.runsup.UPDATE_SPORT_ACTIVITY");
                getActivity().startService(intent);
            }
        });
        alertDisplay.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });


        AlertDialog alert = alertDisplay.create();
        alert.show();
    }

    //-------------------------------------------------

    public void openWorkoutDetailActivity() {
        /*
        //Izprazni tabelo Workout
        try {
            for(Workout workout : workoutDao.queryForAll()){
                workoutDao.delete(workout);
            }
        } catch (SQLException e){
            e.printStackTrace();
        }
        */
       //Ustvarimo nov workout objekt in ga potisnemo na bazo
        Workout workout = new Workout();

        try {
            Log.d(TAG, "WORKOUT ID: " + workoutID.toString());
            workout = workoutDao.queryForId(workoutID);
            //zdej ga je potrebno se ustrezno dopolnit in updejtat
            workout.setDistance(distance);
            workout.setDuration(duration);
            workout.setTotalCalories(calories);
            workout.setPaceAvg(avgSpeed);
            workout.setSportActivity(sportActivity);
            workout.setStatus(Workout.statusEnded);
            workoutDao.createOrUpdate(workout);
        } catch (SQLException e) {
            e.printStackTrace();
        }



        Bundle extras = new Bundle();

        extras.putInt("sportActivity", sportActivity);
        extras.putLong("duration", duration);
        extras.putDouble("distance", distance);
        //extras.putDouble("pace", pace);
        extras.putDouble("calories", calories);
        extras.putDouble("avgSpeed", avgSpeed);
        extras.putSerializable("finalPositionList", postitionLists);
        extras.putLong("workoutId", workoutID);
        //MANJKA SE LOCATION LIST EXTRA!!
        Log.d(TAG, "DURATION SENT: " + duration);
        Intent intent = new Intent(getActivity(), WorkoutDetailActivity.class);
        intent.putExtras(extras);
        startActivity(intent);

        getActivity().stopService(new Intent(getActivity(), TrackerService.class));
    }


    //ZA OPTION MENU DESNO ZGORAJ
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.stopwatch_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(getActivity(), SettingsActivity.class);
            startActivity(intent);

        }else if (id == R.id.action_sync){

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        getActivity().unregisterReceiver(mRecevier);
        if(state == 0){
            Intent intent = new Intent(getActivity(), TrackerService.class);
            getActivity().stopService(intent);
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        getActivity().registerReceiver(mRecevier, mfilter);
        //to si dodau

        Intent intent = new Intent(getActivity(), TrackerService.class);
        getActivity().startService(intent);
        super.onResume();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
