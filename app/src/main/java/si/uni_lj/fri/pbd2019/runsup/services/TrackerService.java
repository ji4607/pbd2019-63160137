package si.uni_lj.fri.pbd2019.runsup.services;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.PowerManager;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.io.Serializable;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;
import si.uni_lj.fri.pbd2019.runsup.helpers.SportActivities;
import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;

public class TrackerService extends Service {
    private static final String TAG = "TrackerService";

    private GoogleMap mMap;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    public static final int REQUEST_ID_LOCATION_PERMISSIONS = 0;
    LocationRequest mLocationRequest;
    LocationCallback mLocationCallback;
    Location mCurrentLocation;
    String mLastUpdateTime;
    boolean mRequestingLocationUpdates = false;

    MainHelper mainHelper = new MainHelper();


    private final IBinder mBinder = new LocalBinder();
    private static Handler mHandler;
    private static int mSportActivity;
    private static int mState;
    private static long mDuration;
    private static double mDistance, mPace, mCalories;
    private static List<Float> speedList;
    private static long lastLocationUpdateTime;

    private static double avgSpeed;

    private long drawPathTime;
    private static ArrayList<Location> positionList;
    private static Location currentLocation;
    private static Location lastPosition;
    private static Location currentPosition;
    private static Boolean resetVariables;
    private static int listSize;



    private static long currentTime;
    Calendar  mCal = Calendar.getInstance();
    private static Boolean mIsPaused;
    private static long mstartTime;
    PowerManager.WakeLock wl;
    private boolean startLocation;
    private SharedPreferences mSharedPref;

    //Dao objekti za bazo
    private Dao<Workout, Long> workoutDao;
    private Dao<GpsPoint, Long> gpsPointsDao;
    private DatabaseHelper databaseHelper;
    private Long workoutID;


    @SuppressLint("InvalidWakeLockTag")
    @Override
    public void onCreate() {
        mHandler = new Handler();

        avgSpeed = 0;
        mState = 0;
        /*
        start = 1
        continue = 3
        pause = 2
        stop = 0
         */
        //INICIALIZIRAJ DAO
        workoutDao = null;
        gpsPointsDao = null;
        databaseHelper = OpenHelperManager.getHelper(TrackerService.this, DatabaseHelper.class);
        try {
            workoutDao = databaseHelper.workoutDao();
            gpsPointsDao = databaseHelper.gpsPointDao();
        }catch (SQLException e) {
            e.printStackTrace();
        }
        workoutID = Long.valueOf(-1);
        mSharedPref = getSharedPreferences("SharedPref", Context.MODE_PRIVATE);
        startLocation = false;

        drawPathTime = 0;
        mSportActivity = 0;
        mIsPaused = true;
        mDuration = 0;
        mCalories = 0;
        mDistance = 0;
        mPace = 0;
        speedList = new ArrayList<Float>();
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        positionList = new ArrayList<Location>();
        lastPosition = null;
        currentPosition = null;
        currentLocation = null;
        resetVariables = false;
        listSize = 1;

        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);
        wl.acquire();
        super.onCreate();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    //dobimo instanco našega servica, na karkoli ga zelimo bindat
    public class LocalBinder extends Binder {
        public TrackerService getService() {
            return TrackerService.this;
        }
    }

    //Funkcija se kliče usakič ko StopwatchActivity kliče startService in pošlje intent, na podlagi tega se odločimo kaj nardit
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "service started on resume");

        if(intent.getAction() != null)
            startAction(intent);

        return super.onStartCommand(intent, flags, startId);
    }

    //Funkcija prične ali ustavi runnable
    public void startAction(Intent intent) {
        String command = null;
        if (intent != null)
            command = intent.getAction();
        Log.d(TAG, "BROADCAST RECIVED WITH ACTION " + command);

        if (command != null && command.equals("si.uni_lj.fri.pbd2019.runsup.COMMAND_START")) {
            mState = 1;
            //start location updates
            //wakelock.
            if (!mRequestingLocationUpdates)
                mRequestingLocationUpdates = true;
            commandStart();
        } else if (command != null && command.equals("si.uni_lj.fri.pbd2019.runsup.COMMAND_CONTINUE")) {
            mState = 3;
            Bundle bundle = intent.getExtras();
            workoutID = bundle.getLong("workoutId");

            Workout workout;
            try {
                workout = workoutDao.queryForId(workoutID);
                mDuration = workout.getDuration();
                mDistance = workout.getDistance();
                mPace = workout.getPaceAvg();
                mCalories = workout.getTotalCalories();

            }catch (SQLException e){
                e.printStackTrace();
            }
            if (!mRequestingLocationUpdates)
                mRequestingLocationUpdates = true;
            commandContinue();
        } else if (command != null && command.equals("si.uni_lj.fri.pbd2019.runsup.COMMAND_PAUSE")) {
            mState = 2;
            //stop location updates
            resetVariables = true;
            commandPause();
        } else if (command != null && command.equals("si.uni_lj.fri.pbd2019.runsup.COMMAND_STOP")) {
            mState = 0;
            commandStop();
        }else if  (command != null && command.equals("si.uni_lj.fri.pbd2019.runsup.UPDATE_SPORT_ACTIVITY")) {
            Bundle extras = intent.getExtras();
            workoutID = extras.getLong("workoutId");
            mSportActivity = intent.getIntExtra("sportActivity", 0);
            if(workoutID.equals(Long.valueOf(-1))){
                Log.d(TAG, "Ne updejtam nic ker ne obstaja se");
            }else {
                Log.d(TAG, "Grem updejtat existing workout");
                Workout workout;
                try {
                    workout = workoutDao.queryForId(workoutID);
                    workout.setSportActivity(mSportActivity);
                    //workout.setStatus(Workout.statusUnknown);
                    //RABIM KEJ UPDEJTAT ZA RACUNANJE?

                    workoutDao.createOrUpdate(workout);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            Log.d(TAG, "Sport activity is equal to " + mSportActivity);
        }
    }

    //Funkcija doda parametre in v bundle in jih pošlje v StopwatchActivity
    public void sendOutBroadcast() {
        Log.d(TAG, "SENT BROADCAST");
        //mDuration += 1000;
        Log.d(TAG, "DURATION: " + mDuration);
        //Log.d(TAG, "DURATION: " + mDuration/1000);
        // 11:37:00
        lastLocationUpdateTime += 1000;
        drawPathTime += 1000;

        currentTime = SystemClock.uptimeMillis();
        mDuration += (currentTime - mstartTime); // 11:37:02
        mstartTime = currentTime;
        // mDuration = 1 + 2
        Log.d(TAG, "POSITION LIST SIZE: " + positionList.size());
        if(positionList.size() > 1 && listSize < positionList.size()){
            Log.d(TAG, "DISTANCE: " + mDistance);
            mDistance += lastPosition.distanceTo(currentPosition);
            listSize++;

            double minutes = (mDuration/1000.0)/60.0;
            double km =  (mDistance/1000.0);
            mPace = minutes/km;

            avgSpeed += mPace;


            double seconds = (mDuration/1000.0);
            Float speed = (float) (mDistance/seconds);
            speedList.add(speed);

        }

        Log.d(TAG, "last location update: " + lastLocationUpdateTime);
        if(lastLocationUpdateTime >= 120000 || mPace > 1000.0){
            Log.d(TAG, "PACE RESETED:");
            mPace = 0.00;
        }

        if(drawPathTime > 15000){
            drawPathTime = 1000;
        }
        String weight = mSharedPref.getString("weight", "60.0");
        double d = new Double(weight).doubleValue();
        Log.d(TAG, "double uspeu" + d);
        //Recalculate calories if speedlist is large enough
        if(speedList.size() > 1){
            double mDurationHours = ((mDuration/1000.0)/60.0)/60.0;
            Log.d(TAG, "mDurationHours: " + mDurationHours);

            mCalories = SportActivities.countCalories(getSportActivity(), 60, speedList, mDurationHours);
        }


        Log.d(TAG, "Calories: " + mCalories);
        Log.d(TAG, "pace: " + getPace());
        Bundle extras = new Bundle();
        //mainHelper.formatDistance(getDistance())
        extras.putLong("pathTime", drawPathTime);
        extras.putLong("workoutID", workoutID);
        extras.putInt("sportActivity", getSportActivity());
        extras.putInt("state", getState());
        extras.putLong("duration", getDuration());
        extras.putDouble("distance", getDistance());
        extras.putDouble("pace", getPace());
        extras.putDouble("calories", getCalories());
        extras.putDouble("avgSpeed", (avgSpeed/speedList.size()));
        extras.putSerializable("positionList", positionList);
        extras.putBoolean("firstLocation", startLocation);

        //manjka se list of gps points

        Intent i = new Intent();
        i.setAction("si.uni_lj.fri.pbd2019.runsup.TICK");
        i.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        i.putExtras(extras);

        i.putExtra("location", currentLocation);
        if(currentLocation != null)
            Log.d(TAG, "longitude " + currentLocation.getLongitude());
        sendBroadcast(i);
    }



    public void startPretendLongRunningTask() {
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (getIsPaused()) {
                    Log.d(TAG, "run: removing callbacks");
                    mHandler.removeCallbacks(this); // remove callbacks from runnable
                    //pausePretendLongRunningTask();
                    //mHandler.postDelayed(this, 1000);
                } else {
                    sendOutBroadcast();
                    mHandler.postDelayed(this, 1000); // continue incrementing
                }
            }
        };
        mHandler.postDelayed(runnable, 1000);
    }

    public void pausePretendLongRunningTask() {
        mIsPaused = true;
        Log.d(TAG, "service is on pause");
    }

    public void unPausePretendLongRunningTask() {
        mIsPaused = false;
        Log.d(TAG, "service is working");
    }


    //---------------------------------------------------------
    //ZA LOCATION FUNKCIJE
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(3000);
        mLocationRequest.setSmallestDisplacement(10);
        mLocationRequest.setFastestInterval(3000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void createLocationCallback() {
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                mCurrentLocation = locationResult.getLastLocation();
                mLastUpdateTime = DateFormat.getTimeInstance().format(new
                        Date());
                updateLocationUI();
            }
        };
    }

    public void updateLocationUI() {
        //showLastKnownLocation(findViewById(R.id.btn_start_Location_Updates));

        showLastKnownLocation();
    }

    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Log.d(TAG, "LOCATION UDATES");
        createLocationRequest();
        createLocationCallback();
        if(Looper.myLooper() == null) {
            Looper.prepare();
        }
        mFusedLocationProviderClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());

    }

    private void stopLocationUpdates() {
        if (!mRequestingLocationUpdates) return;
        mFusedLocationProviderClient.removeLocationUpdates(mLocationCallback)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        mRequestingLocationUpdates = false;

                    }
                });
    }

    public void showLastKnownLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mFusedLocationProviderClient.getLastLocation()
                .addOnSuccessListener(new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        //morem dodat v list
                        lastLocationUpdateTime = 0;
                        startLocation = true;
                        currentLocation = location;

                        //DODAJ LOCATION V BAZO
                        addGpsPoint(location.getLatitude(), location.getLongitude());


                        positionList.add(location);
                        //spremenit current v last in current v location
                        lastPosition = currentPosition;
                        currentPosition = location;

                        Log.d(TAG, "Location update: " + currentLocation.getLatitude() + " " + currentLocation.getLongitude());
                        LatLng currentLatLng = new LatLng(location.getLatitude(),
                                location.getLongitude());
                        //refreshLongLat(location.getLatitude(), location.getLongitude());
                    }
                });

    }

    public void addGpsPoint(double lat, double lng){
        GpsPoint gpsPoint = new GpsPoint();
        Date date = new Date();
        try {
            Workout workout = workoutDao.queryForId(workoutID);
            gpsPoint.setWorkout(workout);
            gpsPoint.setCreated(date);
            gpsPoint.setLongitude(lng);
            gpsPoint.setLatitude(lat);

            gpsPointsDao.createIfNotExists(gpsPoint);
            Log.d(TAG, "DODAN GPS POINT ");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //---------------------------------------------------------

    //FUNKCIJE ZA COMMANDE IZ BROADCASTA
    private void commandStart(){
        /*try {
            for(GpsPoint gps : gpsPointsDao.queryForAll()){
                gpsPointsDao.delete(gps);
            }
        } catch (SQLException e){
            e.printStackTrace();
        }*/


        //Ustvarimo nov workout objekt in ga potisnemo na bazo
        Workout workout = new Workout();
        Date date = new Date();

        try {
            workoutDao.createIfNotExists(workout);
            workoutID = workout.getId();
            Log.d(TAG, "WORKOUT ID: " + workoutID.toString());

            //zdej ga je potrebno se ustrezno dopolnit in updejtat
            workout.setTitle("Workout " + workoutID.toString());
            workout.setCreated(date);
            workout.setStatus(0);
            workout.setSportActivity(mSportActivity);
            workoutDao.createOrUpdate(workout);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        mDuration = 0;
        mstartTime = SystemClock.uptimeMillis(); //11:37:00
        if (mRequestingLocationUpdates) {
            startLocationUpdates();
        }
        unPausePretendLongRunningTask();
        startPretendLongRunningTask();
    }

    private void commandContinue(){
        mstartTime = SystemClock.uptimeMillis();
        if (mRequestingLocationUpdates) {
            startLocationUpdates();
        }
        unPausePretendLongRunningTask();
        startPretendLongRunningTask();
    }

    private void commandPause(){
        positionList = new ArrayList<Location>();
        stopLocationUpdates();
        pausePretendLongRunningTask();

    }

    private void commandStop(){
        stopLocationUpdates();
        pausePretendLongRunningTask();
    }


    public Boolean getIsPaused(){
        return mIsPaused;
    }

    public int getSportActivity(){
        return mSportActivity;
    }

    public int getState(){
       return mState;
    }

    public double getCalories(){
        return mCalories;
    }

    public long getDuration(){
        return mDuration;
    }

    public double getDistance(){
        return mDistance;
    }

    public double getPace(){
        return mPace;
    }

    @Override
    public void onDestroy() {
        wl.release();
        super.onDestroy();

    }

    //ta funkcija se klice, ko je aplikacija odstranjena iz lista nedavno uporabljenih aplikacij
    //service bo deloval, dokler se zadnji client ni unboundau od njega
    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        stopSelf(); //to dodam, zato da se service ugasne, ko se aplikacija ugasne
                    //torej da nebo sevedno service teku, ce tudi aplikacija ni vec uzgana
    }


}
