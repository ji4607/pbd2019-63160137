#
# generated on 2019/05/17 10:44:17
#
# --table-start--
dataClass=si.uni_lj.fri.pbd2019.runsup.model.GpsPoint
tableName=GpsPoint
# --table-fields-start--
# --field-start--
fieldName=id
generatedId=true
# --field-end--
# --field-start--
fieldName=workout
foreign=true
# --field-end--
# --field-start--
fieldName=sessionNumber
# --field-end--
# --field-start--
fieldName=latitude
# --field-end--
# --field-start--
fieldName=longitude
# --field-end--
# --field-start--
fieldName=duration
# --field-end--
# --field-start--
fieldName=speed
# --field-end--
# --field-start--
fieldName=pace
# --field-end--
# --field-start--
fieldName=totalCalories
# --field-end--
# --field-start--
fieldName=created
# --field-end--
# --field-start--
fieldName=lastUpdate
# --field-end--
# --table-fields-end--
# --table-end--
#################################
# --table-start--
dataClass=si.uni_lj.fri.pbd2019.runsup.model.Workout
tableName=Workout
# --table-fields-start--
# --field-start--
fieldName=id
generatedId=true
allowGeneratedIdInsert=true
# --field-end--
# --field-start--
fieldName=user
foreign=true
# --field-end--
# --field-start--
fieldName=title
# --field-end--
# --field-start--
fieldName=created
# --field-end--
# --field-start--
fieldName=status
# --field-end--
# --field-start--
fieldName=distance
# --field-end--
# --field-start--
fieldName=duration
# --field-end--
# --field-start--
fieldName=totalCalories
# --field-end--
# --field-start--
fieldName=paceAvg
# --field-end--
# --field-start--
fieldName=sportActivity
# --field-end--
# --field-start--
fieldName=lastUpdate
# --field-end--
# --table-fields-end--
# --table-end--
#################################
